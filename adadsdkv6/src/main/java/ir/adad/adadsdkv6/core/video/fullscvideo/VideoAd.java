package ir.adad.adadsdkv6.core.video.fullscvideo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import ir.adad.adadsdkv6.BuildConfig;
import ir.adad.adadsdkv6.core.banner.RequestAd;
import ir.adad.adadsdkv6.core.banner.RequestAdCallBack;
import ir.adad.adadsdkv6.core.video.VideoAdFactory;
import ir.adad.adadsdkv6.core.video.VideoAdListener;
import ir.adad.adadsdkv6.models.BaseJsonModel;
import ir.adad.adadsdkv6.models.JsonModelFactory;
import ir.adad.adadsdkv6.models.videoJson.VideoModel;
import ir.adad.adadsdkv6.utils.AdType;
import ir.adad.adadsdkv6.utils.Constants;
import ir.adad.adadsdkv6.utils.HandleNetworkRespons;
import ir.adad.adadsdkv6.utils.PrepareUrl;
import ir.adad.adadsdkv6.utils.Volley.VolleySingleton;


public class VideoAd extends VideoAdFactory implements RequestAdCallBack {

    private final String TAG = "VideoAd";
    private String userId;
    private AdType adType;
    private Context context;
    private WeakReference<Activity> activity;
    private PrepareUrl prepareUrl;
    private RequestAd requestAd;
    private boolean adLoaded = false;
    private VideoAdListener videoAdListener;
    private VideoModel videoModel;
    private long lastMillis = 0;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getStringExtra(Constants.INTENT_EXTRA_STATE)) {
                case Constants.EVENT_SKIPPED:
                    if (videoAdListener != null) {
                        videoAdListener.onAdFinished(false, false, intent.getIntExtra(Constants.INTENT_EXTRA_SKIP_TIME, 0));
                    }
                    break;
                case Constants.EVENT_ACTIVITY_STOPPED:
                    VolleySingleton.getInstance(context.getApplicationContext()).getRequestQueue().cancelAll(TAG);
                    LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
                    if (prepareUrl != null) {
                        prepareUrl.onStop();
                    }
                    if (requestAd != null) {
                        requestAd.onCancel();
                    }
                    break;
                case Constants.EVENT_AD_PLAYED:
                    if (videoAdListener != null) {
                        videoAdListener.onAdStarted();
                    }
                    break;
                case Constants.EVENT_COMPLETED:
                    if (videoAdListener != null) {
                        videoAdListener.onAdFinished(true, false, -1);
                    }
                    break;
                case Constants.EVENT_AD_CLICK:
                    if (videoAdListener != null) {
                        videoAdListener.onAdFinished(true, true, -1);
                    }
                    break;
            }
        }
    };

    public VideoAd(Activity act, AdType adType, String userId) {
        super(act);
        this.context = act;
        this.activity = new WeakReference<>(act);
        this.adType = adType;
        this.userId = userId;

        registerBroadCast();


    }

    private void requestVideoAd() {

        if (!adLoaded) {
            if (lastMillis == 0 || (System.currentTimeMillis() - lastMillis) > 30000) {
                lastMillis = System.currentTimeMillis();
                prepareUrl = PrepareUrl.getInstance(context);
                requestAd = RequestAd.getInstance(context);
                requestAd.setRequestAdListener(this);
                requestAd.sendRequestAd(prepareUrl
                        .url(Constants.BASE_URL, Constants.TYPE_ALLVIDEO, ""));
            }
        }
    }

    public void showAd() {

        if (adLoaded) {
            adLoaded = false;
            activity.get().startActivity(new Intent(activity.get(), ViedoAdActivity.class)
                    .putExtra(Constants.INTENT_EXTRA_TYPE, adType)
                    .putExtra(Constants.INTENT_EXTRA_MODEL, videoModel));
        } else {
            Log.i("ADAD SDK", "The video wasn't loaded yet.");
        }

    }

    public void prepareAd() {

        requestVideoAd();

    }

    public boolean isAdLoaded() {
        return adLoaded;
    }

    public VideoAd setVideoAdListener(VideoAdListener videoAdListener) {
        this.videoAdListener = videoAdListener;
        return this;
    }

    private void registerBroadCast() {
        LocalBroadcastManager
                .getInstance(context)
                .registerReceiver(broadcastReceiver, new IntentFilter(Constants.VIDEO_TYPE_FULLSC));
    }


    @Override
    public void onResponse(AdType adType, BaseJsonModel baseJsonModel) {
        videoModel = (VideoModel) baseJsonModel;
        adLoaded = true;
    }

    @Override
    public void onError(int error) {
        lastMillis = 0;
        if (videoAdListener != null) {
            videoAdListener.onAdFailedToLoad(error);
        }
    }
}
