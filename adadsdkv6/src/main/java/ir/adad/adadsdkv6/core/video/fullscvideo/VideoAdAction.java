package ir.adad.adadsdkv6.core.video.fullscvideo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import java.lang.ref.WeakReference;
import ir.adad.adadsdkv6.models.videoJson.VideoModel;
import ir.adad.adadsdkv6.utils.ADAD;
import ir.adad.adadsdkv6.utils.AdService;
import ir.adad.adadsdkv6.utils.AdType;
import ir.adad.adadsdkv6.utils.Constants;
import ir.adad.adadsdkv6.utils.PrepareUrl;

public class VideoAdAction {

    public VideoAdAction(Context context, String state, AdType adType, VideoModel videoModel, Boolean isFirstTimeCompleted, LocalBroadcastManager localBroadcastManager, Integer skipTime) {
        action(context, state, adType, videoModel, isFirstTimeCompleted, localBroadcastManager, skipTime);
    }


    private void action(Context context, String state, AdType adType, VideoModel videoModel, Boolean isFirstTimeCompleted, LocalBroadcastManager localBroadcastManager, Integer skipTime) {
        Bundle bundle ;
        switch (state) {
            case Constants.EVENT_AD_PLAYED: {
                if (isFirstTimeCompleted && ADAD.testMode != 1) {
                    bundle = new Bundle();
                    bundle.putBoolean(Constants.INTENT_ADAD_CHECK, true);
                    bundle.putSerializable(Constants.INTENT_EXTRA_TYPE, adType);
                    bundle.putString(Constants.INTENT_EXTRA_STATE, Constants.EVENT_AD_PLAYED);
                    bundle.putParcelable(Constants.INTENT_EXTRA_MODEL, videoModel);
                    context.startService(new Intent(context, AdService.class).putExtras(bundle));
                }
                break;
            }
            case Constants.EVENT_SKIPPED: {
                if (isFirstTimeCompleted && ADAD.testMode != 1) {
                    bundle = new Bundle();
                    bundle.putBoolean(Constants.INTENT_ADAD_CHECK, true);
                    bundle.putSerializable(Constants.INTENT_EXTRA_TYPE, adType);
                    bundle.putString(Constants.INTENT_EXTRA_STATE, Constants.EVENT_SKIPPED);
                    bundle.putParcelable(Constants.INTENT_EXTRA_MODEL, videoModel);
                    bundle.putInt(Constants.INTENT_EXTRA_SKIP_TIME, skipTime);
                    context.startService(new Intent(context, AdService.class).putExtras(bundle));
                }
                localBroadcastManager
                        .sendBroadcast(new Intent(Constants.VIDEO_TYPE_FULLSC)
                                .putExtra(Constants.INTENT_EXTRA_STATE, Constants.EVENT_SKIPPED)
                                .putExtra(Constants.INTENT_EXTRA_SKIP_TIME, skipTime));
                break;
            }
            case Constants.EVENT_AD_CLOSED: {
                localBroadcastManager
                        .sendBroadcast(new Intent(Constants.VIDEO_TYPE_FULLSC)
                                .putExtra(Constants.INTENT_EXTRA_STATE,Constants.EVENT_AD_CLOSED));
                break;
            }
            case Constants.EVENT_AD_CLICK: {
                localBroadcastManager
                        .sendBroadcast(new Intent(Constants.VIDEO_TYPE_FULLSC)
                                .putExtra(Constants.INTENT_EXTRA_STATE, Constants.EVENT_AD_CLICK));
                context.startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(videoModel.getHitUrl() + PrepareUrl
                                .getInstance(context.getApplicationContext())
                                .url("",Constants.TYPE_ALLVIDEO, String.valueOf(videoModel.getAdId())))));
                break;
            }
            case Constants.EVENT_COMPLETED: {
                if (isFirstTimeCompleted && ADAD.testMode != 1) {
                    bundle = new Bundle();
                    bundle.putBoolean(Constants.INTENT_ADAD_CHECK, true);
                    bundle.putSerializable(Constants.INTENT_EXTRA_TYPE, adType);
                    bundle.putString(Constants.INTENT_EXTRA_STATE, Constants.EVENT_COMPLETED);
                    bundle.putParcelable(Constants.INTENT_EXTRA_MODEL, videoModel);
                    context.startService(new Intent(context, AdService.class).putExtras(bundle));
                    localBroadcastManager
                            .sendBroadcast(new Intent(Constants.VIDEO_TYPE_FULLSC)
                                    .putExtra(Constants.INTENT_EXTRA_STATE, Constants.EVENT_COMPLETED));
                }
                break;
            }
            case Constants.EVENT_VIDEO_RICH_VIEW: {
                if (isFirstTimeCompleted && ADAD.testMode != 1) {
                    bundle = new Bundle();
                    bundle.putBoolean(Constants.INTENT_ADAD_CHECK, true);
                    bundle.putSerializable(Constants.INTENT_EXTRA_TYPE, adType);
                    bundle.putString(Constants.INTENT_EXTRA_STATE, Constants.EVENT_VIDEO_RICH_VIEW);
                    bundle.putParcelable(Constants.INTENT_EXTRA_MODEL, videoModel);
                    context.startService(new Intent(context, AdService.class).putExtras(bundle));
                }
                break;
            }
            case Constants.EVENT_ACTIVITY_STOPPED:{
                localBroadcastManager
                        .sendBroadcast(new Intent(Constants.VIDEO_TYPE_FULLSC)
                                .putExtra(Constants.INTENT_EXTRA_STATE, Constants.EVENT_ACTIVITY_STOPPED));
                break;
            }

        }
    }
}
