package ir.adad.adadsdkv6.core.video;

import android.app.Activity;

import ir.adad.adadsdkv6.core.video.fullscvideo.VideoAd;
import ir.adad.adadsdkv6.utils.AdType;


public class VideoAdFactory {

    private AdType adType;
    private Activity activity;
    private String userId;

    public VideoAdFactory(Activity act) {
        activity = act;
    }

    public static VideoAdFactory getInstance(Activity act) {
        return new VideoAdFactory(act);

    }

    public VideoAdFactory setAdType(AdType adType) {
        this.adType = adType;
        return this;
    }

    public VideoAdFactory setUserId(String userId){
        this.userId = userId ;
        return this ;
    }

    public <T extends VideoAdFactory> T build() {

        switch (adType) {
            case FULLSCVIDEO:
                return (T) new VideoAd(activity, AdType.FULLSCVIDEO , userId);
            case REWARDEDVIDEO:
                return (T) new VideoAd(activity, AdType.REWARDEDVIDEO , userId);
            default:
                return null;
        }
    }
}
