package ir.adad.adadsdkv6.utils;


public class HandleNetworkRespons {

    public String getString(String cause) {

        switch (cause) {
            case "DIG":
                return "Invalid Request";
            case "MIS":
                return "Missing Data";
            case "TKN":
                return "Token is not set (Check your layout XML)";
            case "MSR":
                return "None";
            case "SSR":
                return "None";
            case "OLD":
                return "Client version is too old";
            case "PEN":
                return "Medium is not accepted by admin yet";
            case "REJ":
                return "Medium has been rejected by admin";
            case "NOA":
                return "None";
            case "IVT":
                return "Token is wrong";
            case "WRT":
                return "Token is wrong";
            case "WPN":
                return "Package name is wrong";
            default:
                return "";
        }
    }


    public int getInt(String cause) {

        switch (cause) {
            case "DIG":
                return 1;
            case "MIS":
                return 2;
            case "TKN":
                return 3;
            case "MSR":
                return 4;
            case "SSR":
                return 5;
            case "OLD":
                return 6;
            case "PEN":
                return 7;
            case "REJ":
                return 8;
            case "NOA":
                return 9;
            case "IVT":
                return 10;
            case "WRT":
                return 11;
            case "WPN":
                return 12;
            default:
                return 0;
        }
    }
}
