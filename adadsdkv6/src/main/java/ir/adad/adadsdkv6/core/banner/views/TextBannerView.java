package ir.adad.adadsdkv6.core.banner.views;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import ir.adad.adadsdkv6.R;
import ir.adad.adadsdkv6.models.bannerJson.TextModel;

import ir.adad.adadsdkv6.utils.Util;

final class TextBannerView extends RelativeLayout {

    private Boolean FLAG = true;
    private Context myContext;
    private TextModel textModel;

    public TextBannerView(Context context, TextModel textModel) {
        super(context);
        this.myContext = context;
        this.textModel = textModel;

    }

    public TextBannerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextBannerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init(ImageView closeImageView, ImageView adadLogoImageView) {

        inflate(getContext(), R.layout.text_banner_view_new, this);

        ImageView logoImageView = findViewById(R.id.textBanner_logo_imageView);
        TextView titleTextView = findViewById(R.id.textBanner_title_textView);
        TextView subtitleTextView = findViewById(R.id.textBanner_subtitle_textView);
        ImageView ctaImageVIew = findViewById(R.id.textBanner_cta_imageView);

        setBackgroundColor(Color.parseColor(textModel.getTextBackgroundColor()));

        if (textModel.getLogo().isEmpty()) {

            logoImageView.setVisibility(INVISIBLE);

            setMarginOnTextViews(titleTextView,subtitleTextView,closeImageView);

        } else {

            setMarginOnLogoImageView(logoImageView,closeImageView);

            Glide.with(myContext)
                    .load(textModel.getLogo())
                    .apply(new RequestOptions().override((int) (Util.getScreenWidth() * 0.1), (int) (Util.getScreenWidth() * 0.1)))
                    .into(logoImageView);
        }

        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, (float) (Util.getScreenWidth() * 0.04 / getResources().getDisplayMetrics().density));
        titleTextView.setText(textModel.getTitle());
        titleTextView.setTextColor(Color.parseColor(textModel.getTextColor()));

        subtitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, (float) (Util.getScreenWidth() * 0.035 / getResources().getDisplayMetrics().density));
        subtitleTextView.setText(textModel.getSubtitle().get(0));
        subtitleTextView.setTextColor(Color.parseColor(textModel.getTextColor()));
        subtitleTextViewAnimated(subtitleTextView, textModel);

        RelativeLayout.LayoutParams ctaLayoutParams = (RelativeLayout.LayoutParams) ctaImageVIew.getLayoutParams();
        ctaLayoutParams.leftMargin = adadLogoImageView.getWidth() + 10;
        ctaImageVIew.setLayoutParams(ctaLayoutParams);

        Glide.with(myContext)
                .load(textModel.getButtonIcon())
                .apply(new RequestOptions().override((int) (Util.getScreenWidth() * 0.1), (int) (Util.getScreenWidth() * 0.1)))
                .into(ctaImageVIew);

    }

    private void setMarginOnLogoImageView(ImageView logoImageView, ImageView closeImageView) {

        RelativeLayout.LayoutParams logoLayoutParams = (RelativeLayout.LayoutParams) logoImageView.getLayoutParams();
        logoLayoutParams.rightMargin = closeImageView.getWidth() + 5;
        logoImageView.setLayoutParams(logoLayoutParams);
    }

    private void setMarginOnTextViews(TextView titleTextView, TextView subtitleTextView, ImageView closeImageView) {

        RelativeLayout.LayoutParams titleLayoutParams = (RelativeLayout.LayoutParams) titleTextView.getLayoutParams();
        titleLayoutParams.rightMargin = closeImageView.getWidth() + 15;
        titleTextView.setLayoutParams(titleLayoutParams);

        RelativeLayout.LayoutParams subtitleLayoutParams = (RelativeLayout.LayoutParams) subtitleTextView.getLayoutParams();
        subtitleLayoutParams.rightMargin = closeImageView.getWidth() + 15;
        subtitleTextView.setLayoutParams(subtitleLayoutParams);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(MeasureSpec.makeMeasureSpec((int) (Util.getScreenWidth() * 0.9), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec((int) (Util.getScreenWidth() * 0.135), MeasureSpec.EXACTLY));

    }

    private void subtitleTextViewAnimated(final TextView subtitleTextView, final TextModel textModel) {
        subtitleTextView
                .animate()
                .alpha(0)
                .setDuration(Util.secToMilisec(1.5))
                .setListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {


                        String subtitle = textModel.getSubtitle().get(0);

                        String alternateText = textModel.getSubtitle().get(1);


                        if (alternateText != "" && FLAG) {
                            subtitleTextView.setText(alternateText);
                            FLAG = false;

                        } else {
                            subtitleTextView.setText(subtitle);
                            FLAG = true;
                        }


                        subtitleTextView.animate().alpha(1).setDuration(Util.secToMilisec(1)).setListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                subtitleTextViewAnimated(subtitleTextView, textModel);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
    }

}
