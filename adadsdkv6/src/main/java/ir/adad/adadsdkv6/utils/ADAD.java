package ir.adad.adadsdkv6.utils;

public class ADAD {

    public static String AdadToken= "";
    public static String userId = "";
    public static int testMode = 0;

    public static void init(String token) {
        AdadToken = token;
    }

    public static void setUserIdentifier(String userIdentifier){
        userId = userIdentifier ;
    }

    public static void setTestMode(int mode){
        testMode = mode ;
    }
}
