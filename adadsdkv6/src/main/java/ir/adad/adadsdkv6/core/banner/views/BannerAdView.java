package ir.adad.adadsdkv6.core.banner.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import ir.adad.adadsdkv6.R;
import ir.adad.adadsdkv6.core.banner.BannerAdListener;
import ir.adad.adadsdkv6.core.banner.RequestAd;
import ir.adad.adadsdkv6.core.banner.RequestAdCallBack;
import ir.adad.adadsdkv6.models.BaseJsonModel;
import ir.adad.adadsdkv6.utils.AdType;
import ir.adad.adadsdkv6.models.bannerJson.BannerModel;
import ir.adad.adadsdkv6.models.bannerJson.TextModel;
import ir.adad.adadsdkv6.utils.ADAD;
import ir.adad.adadsdkv6.utils.AdService;
import ir.adad.adadsdkv6.utils.Constants;
import ir.adad.adadsdkv6.utils.PrepareUrl;
import ir.adad.adadsdkv6.utils.Util;


public final class BannerAdView extends RelativeLayout implements View.OnClickListener, RequestAdCallBack {

    private Context context;
    private ImageView closeImageView;
    private ImageView adadLogoImageView;
    private ImageView imageAdView;
    private TextBannerView textBannerView;
    private Boolean autoBannerView = true;
    private BannerAdListener userBannerAdListener;
    private ObjectAnimator logoAnimate;
    private AdType bannerType;
    private BannerModel bannerModel;
    private TextModel textModel;
    private boolean checkResume;
    private int delay; //milliseconds
    private Handler handler;
    private Runnable runnable;
    private RelativeLayout actionBannerView;
    private Button actButtonYes, actButtonNO;
    private PrepareUrl prepareUrl;
    private RequestAd requestAd;


    @IdRes
    private final int SKIP_IV_ID = 0;
    @IdRes
    private final int ADAD_LG_ID = 1;
    @IdRes
    private final int BANNER_VIEW_ID = 2;
    @IdRes
    private final int CONS_VIEW_ID = 3;
    @IdRes
    private final int YES_BUTTON_ID = 4;
    @IdRes
    private final int NO_BUTTON_ID = 5;

    private String userId;
    private boolean ANETWORk = false;


    public BannerAdView(Context context, Boolean autoBannerView) {
        super(context);
        this.context = context;
        this.autoBannerView = autoBannerView;
        if (autoBannerView) {
            requestBanner(context);
        }

    }

    public BannerAdView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BannerAdView);
        this.autoBannerView = a.getBoolean(R.styleable.BannerAdView_autoBannerView, true);
        if (autoBannerView) {
            requestBanner(context);
        }
        a.recycle();
    }

    public void loadAd() {
        if (!autoBannerView) {
            requestBanner(context);
        }
    }

    public void setBannerAdListener(BannerAdListener userBannerAdListener) {
        this.userBannerAdListener = userBannerAdListener;
    }

    private void requestBanner(Context context) {
        setVisibility(GONE);

        prepareUrl = PrepareUrl.getInstance(context);
        requestAd = RequestAd.getInstance(context).setRequestAdListener(this);
        requestAd.sendRequestAd(prepareUrl.url(Constants.BASE_URL, Constants.TYPE_BANNER, ""));

    }

    private void prepareAd(BaseJsonModel baseJsonModel) {

        removeAllViewsInLayout();
        init(baseJsonModel);
        addBanner();
    }

    private void init(BaseJsonModel baseJsonModel) {


        adadLogoImageView = new ImageView(context);
        closeImageView = new ImageView(context);

        if (bannerType == AdType.TEXTBANNER) {
            textModel = (TextModel) baseJsonModel;
            delay = textModel.getRefreshInterval() * 1000;
            textBannerView = new TextBannerView(context, textModel);

        } else if (bannerType == AdType.IMAGEBANNER) {
            bannerModel = (BannerModel) baseJsonModel;
            delay = bannerModel.getRefreshInterval() * 1000;
            imageAdView = new ImageView(context);
        }

        actionBannerView = new ActionBannerView(context);
        actionBannerView.setVisibility(GONE);
        prepareActionView(context);


        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                removeAllViewsInLayout();
                requestBanner(context);
            }
        };
    }


    private void addBanner() {

        animate().setInterpolator(new DecelerateInterpolator()).alpha(1).translationY(0).setDuration(300).start();
        setId(CONS_VIEW_ID);

        if (bannerType == AdType.TEXTBANNER) {

            Glide.with(context)
                    .load(textModel.getLogoAdad())
                    .apply(new RequestOptions().override((int) (Util.getScreenWidth() * 0.04), (int) (Util.getScreenWidth() * 0.135)))
                    .into(adadLogoImageView);

            displayBanner(textBannerView);

        } else {

            Glide.with(context)
                    .load(bannerModel.getLogoAdad())
                    .apply(new RequestOptions().override((int) (Util.getScreenWidth() * 0.09), (int) (Util.getScreenWidth() * 0.18)))
                    .into(adadLogoImageView);

            Glide.with(context)
                    .load(bannerModel.getImgUrl())
                    .apply(new RequestOptions().override((int) (Util.getScreenWidth() * 0.9), (int) (Util.getScreenWidth() * 0.135)))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                            if (userBannerAdListener != null) {
                                userBannerAdListener.onAdFailedToLoad(0);
                            }
                            requestBanner(context);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            displayBanner(imageAdView);
                            return false;
                        }
                    })
                    .into(imageAdView);
        }

        RelativeLayout.LayoutParams logoAdadParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        logoAdadParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        logoAdadParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        adadLogoImageView.setLayoutParams(logoAdadParams);
        adadLogoImageView.setId(ADAD_LG_ID);
        adadLogoImageView.setOnClickListener(this);
        prepareLogoAnimate();

        closeImageView.setImageDrawable(getResources().getDrawable(R.drawable.banner_ic_skip));
        RelativeLayout.LayoutParams closeImageViewParams = new RelativeLayout.LayoutParams((int) (Util.getScreenWidth() * 0.045), (int) (Util.getScreenWidth() * 0.045));
        closeImageViewParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        closeImageViewParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        closeImageView.setPadding(10, 10, 10, 10);
        closeImageView.setLayoutParams(closeImageViewParams);
        closeImageView.setId(SKIP_IV_ID);
        closeImageView.setOnClickListener(this);

        closeImageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            private boolean oneTime = true;

            @Override
            public void onGlobalLayout() {


                if (adadLogoImageView.getWidth() != 0 && oneTime && bannerType == AdType.TEXTBANNER) {
                    oneTime = false;
                    textBannerView.init(closeImageView, adadLogoImageView);
                }

            }
        });
    }

    private void displayBanner(View view) {

        RelativeLayout.LayoutParams bannerViewParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        bannerViewParams.addRule(RelativeLayout.ALIGN_RIGHT, CONS_VIEW_ID);
        bannerViewParams.addRule(RelativeLayout.ALIGN_LEFT, CONS_VIEW_ID);
        bannerViewParams.addRule(RelativeLayout.ALIGN_TOP, CONS_VIEW_ID);
        bannerViewParams.addRule(RelativeLayout.ALIGN_BOTTOM, CONS_VIEW_ID);


        view.setLayoutParams(bannerViewParams);
        view.setId(BANNER_VIEW_ID);
        view.setOnClickListener(this);

        setVisibility(VISIBLE);
        addView(view);
        addView(adadLogoImageView);
        addView(actionBannerView);
        addView(closeImageView);

        impressionState();
    }

    private void prepareActionView(Context context) {

        // Create Action View for AnetWork
        actionBannerView = new ActionBannerView(context);
        actionBannerView.setVisibility(GONE);

        // Find and setTextSize action TextView
        TextView actTextTilte = actionBannerView.findViewById(R.id.action_textView);
        actTextTilte.setTextSize(TypedValue.COMPLEX_UNIT_DIP, (float) (Util.getScreenWidth() * 0.035 / getResources().getDisplayMetrics().density));

        // Find And setTextSize action Button Yes
        actButtonYes = actionBannerView.findViewById(R.id.action_button_yes);
        actButtonYes.setOnClickListener(this);
        actButtonYes.setTextSize(TypedValue.COMPLEX_UNIT_DIP, (float) (Util.getScreenWidth() * 0.03 / getResources().getDisplayMetrics().density));
        actButtonYes.setId(YES_BUTTON_ID);

        // Find And setTextSize action Button No
        actButtonNO = actionBannerView.findViewById(R.id.action_button_no);
        actButtonNO.setOnClickListener(this);
        actButtonNO.setTextSize(TypedValue.COMPLEX_UNIT_DIP, (float) (Util.getScreenWidth() * 0.03 / getResources().getDisplayMetrics().density));
        actButtonNO.setId(NO_BUTTON_ID);

    }

    private void impressionState() {

        if (autoBannerView && handler != null) {
            handler.postDelayed(runnable, delay);
        }

        if (userBannerAdListener != null) {
            userBannerAdListener.onAdShow();
        }

        if (ADAD.testMode != 1) {

            if (bannerType == AdType.TEXTBANNER) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.INTENT_ADAD_CHECK, true);
                bundle.putSerializable(Constants.INTENT_EXTRA_TYPE, AdType.TEXTBANNER);
                bundle.putString(Constants.INTENT_EXTRA_STATE, Constants.EVENT_AD_PLAYED);
                bundle.putParcelable(Constants.INTENT_EXTRA_MODEL, textModel);
                context.startService(new Intent(context, AdService.class).putExtras(bundle));

            } else {

                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.INTENT_ADAD_CHECK, true);
                bundle.putSerializable(Constants.INTENT_EXTRA_TYPE, AdType.IMAGEBANNER);
                bundle.putString(Constants.INTENT_EXTRA_STATE, Constants.EVENT_AD_PLAYED);
                bundle.putParcelable(Constants.INTENT_EXTRA_MODEL, bannerModel);
                context.startService(new Intent(context, AdService.class).putExtras(bundle));

                if (!bannerModel.getPixelUrl().isEmpty()) {
                    Bundle bundle1 = new Bundle();
                    bundle1.putBoolean(Constants.INTENT_ADAD_CHECK, true);
                    bundle1.putSerializable(Constants.INTENT_EXTRA_TYPE, AdType.IMAGEBANNER);
                    bundle1.putString(Constants.INTENT_EXTRA_STATE, Constants.EVENT_API_AD_PLAYED);
                    bundle1.putParcelable(Constants.INTENT_EXTRA_MODEL, bannerModel);
                    context.startService(new Intent(context, AdService.class).putExtras(bundle1));

                }
            }
        }
    }

    private void prepareLogoAnimate() {

        logoAnimate = ObjectAnimator.ofFloat(adadLogoImageView, "alpha", 0f, 1f);
        logoAnimate.setRepeatMode(ObjectAnimator.REVERSE);
        logoAnimate.setRepeatCount(ObjectAnimator.INFINITE);
        logoAnimate.setDuration(4000);
        logoAnimate.start();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(MeasureSpec.makeMeasureSpec((int) (Util.getScreenWidth() * 0.9), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec((int) (Util.getScreenWidth() * 0.135), MeasureSpec.EXACTLY));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case SKIP_IV_ID: {
                closeState();
                break;
            }
            case ADAD_LG_ID: {
                adadLogoClicked();
                break;
            }

            case BANNER_VIEW_ID: {

                if (ANETWORk)
                    actionBannerView.setVisibility(VISIBLE);
                else {
                    clickState();
                }
                break;

            }
            case YES_BUTTON_ID: {
                clickState();
                actionBannerView.setVisibility(GONE);
                break;
            }

            case NO_BUTTON_ID: {
                actionBannerView.setVisibility(GONE);
                break;

            }
        }
    }

    private void adadLogoClicked() {
    }

    private void closeState() {


        if (userBannerAdListener != null) {
            userBannerAdListener.onAdClosed();
        }

        animate().setInterpolator(new AccelerateInterpolator()).alpha(0).translationY(getHeight()).setDuration(300).start();
        setVisibility(GONE);
        logoAnimate.cancel();

    }

    private void clickState() {

        if (userBannerAdListener != null) {
            userBannerAdListener.onAdClicked();
        }


        if (bannerType == AdType.TEXTBANNER) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(textModel.getHitUrl() +
                    PrepareUrl.getInstance(context)
                            .url("", Constants.TYPE_BANNER, String.valueOf(textModel.getAdId()))));
            context.startActivity(browserIntent);
        } else {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(bannerModel.getHitUrl() +
                    PrepareUrl.getInstance(context)
                            .url("", Constants.TYPE_BANNER, String.valueOf(bannerModel.getAdId()))));
            context.startActivity(browserIntent);
        }
    }

    public void onStop() {

        removeAllViewsInLayout();

        if (requestAd != null) {
            requestAd.onCancel();
        }

        if (prepareUrl != null) {
            prepareUrl.onStop();
        }

        if (handler != null) {
            handler.removeCallbacks(runnable);
        }

        checkResume = true;
    }

    public void onResume() {

        if (checkResume && context != null) {
            requestBanner(context);
        }

        checkResume = false;
    }

    public void onPause() {

    }

    public BannerAdView setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public void disableBannerAds() {
        onStop();
    }

    public void enableBannerAds() {
        onResume();
    }

    @Override
    public void onResponse(AdType adType, BaseJsonModel baseJsonModel) {

        bannerType = adType;
        prepareAd(baseJsonModel);

        if (userBannerAdListener != null) {
            userBannerAdListener.onAdAvailable();
        }

    }

    @Override
    public void onError(int error) {

        if (userBannerAdListener != null) {
            userBannerAdListener.onAdFailedToLoad(error);
        }
    }
}

