package ir.adad.adadsdkv6.core.banner;

import ir.adad.adadsdkv6.models.BaseJsonModel;
import ir.adad.adadsdkv6.utils.AdType;

/**
 * Created by saeed on 12/25/17.
 */

public interface RequestAdCallBack {

    public void onResponse(AdType adType ,BaseJsonModel baseJsonModel);
    public void onError(int error);
}
