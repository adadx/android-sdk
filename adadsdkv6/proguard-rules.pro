# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/saeed/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


##Glide
-keep class com.bumptech.glide.integration.volley.VolleyGlideModule
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-keep public class ir.adad.adadsdkv6.core.banner.views.BannerAdView {
    public <init>(android.content.Context, java.lang.Boolean);
    public <init>(android.content.Context, android.util.AttributeSet);
    public *;
}

-keep public class ir.adad.adadsdkv6.utils.AdServiceRequest {
public *;
}

-keep public class ir.adad.adadsdkv6.core.interstitial.InterstitialAction {
public *;
}

-keep public class ir.adad.adadsdkv6.core.video.fullscvideo.VideoAdAction {
public *;
}

-keep public class ir.adad.adadsdkv6.core.interstitial.InterstitialAd {
public *;
}

-keep public class ir.adad.adadsdkv6.core.video.VideoAdFactory {
public *;
}

-keep public class ir.adad.adadsdkv6.core.video.fullscvideo.VideoAd {
public *;
}

-keep public class ir.adad.adadsdkv6.utils.ADAD {
public *;
}

-keep class ir.adad.adadsdkv6.utils.GlideApp

-keep public interface ir.adad.adadsdkv6.core.banner.BannerAdListener{
public *;
}
-keep public interface ir.adad.adadsdkv6.core.interstitial.InterstitialAdListener{
public *;
}
-keep public interface ir.adad.adadsdkv6.core.video.VideoAdListener{
public *;
}

-keep public enum ir.adad.adadsdkv6.utils.AdType{
 *;
}





