package ir.adad.adadsdkv6.core.interstitial;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import ir.adad.adadsdkv6.R;
import ir.adad.adadsdkv6.models.interstitial.InterstitialModel;
import ir.adad.adadsdkv6.utils.Constants;

import ir.adad.adadsdkv6.utils.Util;


public final class InterstitialActivity extends AppCompatActivity {

    private LocalBroadcastManager localBroadcastManager;
    private ImageView bannerImageView;
    private ImageView skipImageView;
    private ImageView adadLogoImageView;
    private RelativeLayout interstitialConsLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial_new);
        init();
    }

    private void init() {

        InterstitialModel interstitialModel = getIntent().getParcelableExtra(Constants.INTENT_EXTRA_MODEL);
        interstitialConsLayout = findViewById(R.id.interstitial_consLayout);
        bannerImageView = findViewById(R.id.interstitial_banner_imageView);
        skipImageView = findViewById(R.id.interstitial_skip_imageView);
        adadLogoImageView = findViewById(R.id.interstitial_adadlogo_imageView);
        localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());

        prepareView(interstitialModel);
    }

    private void hideSystemUI() {

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }

    private void prepareView(final InterstitialModel interstitialModel) {

        interstitialConsLayout.setBackgroundColor(Color.parseColor(interstitialModel.getBackgroundColor()));

        Glide.with(this)
                .load(R.drawable.video_ic_skip)
                .apply(new RequestOptions().override((int) (Util.getScreenWidth() * 0.05), (int) (Util.getScreenWidth() * 0.05)))
                .into(skipImageView);


        Glide.with(this)
                .load(interstitialModel.getLogoAdad())
                .apply(new RequestOptions().override((int) (Util.getScreenWidth() * 0.2), (int) (Util.getScreenWidth() * 0.4)))
                .into(adadLogoImageView);

        Glide.with(this)
                .load(interstitialModel.getImgUrl())
                .listener(new RequestListener<Drawable>() {

                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        AdShowState(interstitialModel);
                        return false;
                    }
                })
                .apply(new RequestOptions().override(Util.getScreenWidth(), Util.getScreenWidth()))
                .into(bannerImageView);

        skipImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closedState();
            }
        });

        bannerImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickState(interstitialModel);
            }
        });
    }

    private void AdShowState(InterstitialModel interstitialModel) {
        action(this,Constants.EVENT_AD_PLAYED,interstitialModel,localBroadcastManager);
    }

    private void clickState(InterstitialModel interstitialModel) {
        action(this,Constants.EVENT_AD_CLICK,interstitialModel,localBroadcastManager);
        finish();
    }

    private void closedState() {
        action(this,Constants.EVENT_AD_CLOSED,null,localBroadcastManager);
        finish();

    }

    private void action(Context context, String state, InterstitialModel interstitialModel, LocalBroadcastManager localBroadcastManager){

        new InterstitialAction(context,state,interstitialModel,localBroadcastManager);

    }

    @Override
    protected void onStop() {
        super.onStop();
        action(this,Constants.EVENT_ACTIVITY_STOPPED,null,localBroadcastManager);
        Glide.with(this).clear(bannerImageView);

    }

    @Override
    protected void onResume() {
        super.onResume();
        hideSystemUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
