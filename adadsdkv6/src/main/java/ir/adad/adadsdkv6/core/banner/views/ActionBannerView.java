package ir.adad.adadsdkv6.core.banner.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import ir.adad.adadsdkv6.R;
import ir.adad.adadsdkv6.utils.Util;

/**
 * Created by saeed on 12/18/17.
 */

class ActionBannerView extends RelativeLayout {


    public ActionBannerView(Context context) {
        super(context);
        init(context);
    }

    public ActionBannerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ActionBannerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        inflate(getContext(), R.layout.action_banner_view, this);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(MeasureSpec.makeMeasureSpec((int) (Util.getScreenWidth() * 0.9), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec((int) (Util.getScreenWidth() * 0.135), MeasureSpec.EXACTLY));

    }
}
