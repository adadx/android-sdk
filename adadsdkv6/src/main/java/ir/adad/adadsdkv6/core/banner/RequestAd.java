package ir.adad.adadsdkv6.core.banner;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import ir.adad.adadsdkv6.BuildConfig;
import ir.adad.adadsdkv6.models.JsonModelFactory;
import ir.adad.adadsdkv6.utils.AdType;
import ir.adad.adadsdkv6.utils.Constants;
import ir.adad.adadsdkv6.utils.HandleNetworkRespons;
import ir.adad.adadsdkv6.utils.Volley.VolleySingleton;


public class RequestAd {

    private RequestAdCallBack requestAdCallBack;
    private Context context;
    private final String REQUESTADTAG = "adadRequestAd ";

    private RequestAd(Context context) {
        this.context = context;
    }

    public static synchronized RequestAd getInstance(Context context) {
        return new RequestAd(context);
    }


    public RequestAd setRequestAdListener(RequestAdCallBack requestAdCallBack) {
        this.requestAdCallBack = requestAdCallBack;
        return this;
    }

    public void sendRequestAd(String url) {

        VolleySingleton.getInstance(context)
                .addToRequestQueue(new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responseJsonObject) {
                        try {
                            switch (responseJsonObject.getString(Constants.CONTENTTYPE)) {
                                case Constants.TYPE_TEXTBANNER:
                                    requestAdCallBack.onResponse(AdType.TEXTBANNER, JsonModelFactory.getInstants(responseJsonObject));
                                    break;
                                case Constants.TYPE_IMAGEBANNER:
                                    requestAdCallBack.onResponse(AdType.IMAGEBANNER, JsonModelFactory.getInstants(responseJsonObject));
                                    break;
                                case Constants.TYPE_IMAGEINTERSTITIAL:
                                    requestAdCallBack.onResponse(null, JsonModelFactory.getInstants(responseJsonObject));
                                    break;
                                case Constants.TYPE_VIDEO:
                                    requestAdCallBack.onResponse(null, JsonModelFactory.getInstants(responseJsonObject));
                                    break;
                                case Constants.TYPE_HYBRID:
                                    requestAdCallBack.onResponse(null, JsonModelFactory.getInstants(responseJsonObject));
                                    break;
                            }


                        } catch (JSONException e) {
                            if (BuildConfig.DEBUG)
                                e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error.getCause() != null && error.getCause().getMessage() != null) {
                            requestAdCallBack.onError(new HandleNetworkRespons().getInt(error.getCause().getMessage()));
                        }

                    }
                }) {
                    @Override
                    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                        if (response.statusCode == 200)
                            return super.parseNetworkResponse(response);
                        else
                            return Response.error(new VolleyError(new Throwable(response.headers.get("Cause"))));
                    }
                }.setTag(REQUESTADTAG));
    }

    public void onCancel() {

        VolleySingleton.getInstance(context).getRequestQueue().cancelAll(REQUESTADTAG);
    }


}
