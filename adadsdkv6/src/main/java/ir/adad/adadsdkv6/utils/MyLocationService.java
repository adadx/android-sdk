package ir.adad.adadsdkv6.utils;

import android.Manifest;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.Locale;

import static ir.adad.adadsdkv6.utils.Constants.ADAD_LOCATION;
import static ir.adad.adadsdkv6.utils.Constants.BAZAAR_DEFAULT_LOCATION_URI;

public class MyLocationService extends Service implements LocationListener {


    private final long LOCATION_REFRESH_TIME = 100;
    private final float LOCATION_REFRESH_DISTANCE = 1000;
    private String TAG = getClass().getSimpleName();
    private boolean mSuppressBazaarErrors = false;
    private LocationManager locationManager = null;
    private SharedPreferences preferences;
    private Criteria criteria;

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");

        initializeLocationManager();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        criteria = createCoarseCriteria();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria, true), LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, this);
        } else {
            Log.i(TAG, "LocationMethods: NO PERMISSION GRANTED ");
        }

        getDeviceLocation(BAZAAR_DEFAULT_LOCATION_URI);

    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (locationManager == null) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private String getDeviceLocation(String bazaarUri) {

        String deviceLocation = getLocationFromBazaar(bazaarUri);
        if (deviceLocation != null) {
            return deviceLocation;
        }
        deviceLocation = getLocationFromAndroid();
        return deviceLocation != null ? deviceLocation : "";
    }

    private String getLocationFromBazaar(String bazaarUri) {
        ContentResolver contentResolver = getContentResolver();
        Uri bazaarUidUri = Uri.parse(bazaarUri);
        String loc = null;

        try {
            Cursor cursor = contentResolver.query(bazaarUidUri, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                loc = cursor.getString(0);
            }
        } catch (Exception ignored) {
        }

        if (!mSuppressBazaarErrors) {
            mSuppressBazaarErrors = true;
        }
        return loc;
    }

    private String getLocationFromAndroid() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        Location finalLoc;
        String result = null;

        finalLoc = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));

        if (finalLoc != null) {
            result = String.format(
                    Locale.ENGLISH,
                    "%f_%f",
                    finalLoc.getLatitude(),
                    finalLoc.getLongitude());

        } else if (preferences.getString(ADAD_LOCATION, null) != null) {
            result = preferences.getString(ADAD_LOCATION, null);

            locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria, true), LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, this);

        }

        return result;
    }

    public Criteria createCoarseCriteria() {

        Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_COARSE);
        c.setAltitudeRequired(false);
        c.setBearingRequired(false);
        c.setSpeedRequired(false);
        c.setCostAllowed(true);
        c.setPowerRequirement(Criteria.POWER_HIGH);
        return c;

    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (locationManager != null) {
            try {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager.removeUpdates(this);
                }
            } catch (Exception ex) {
                Log.i(TAG, "fail to remove location listners, ignore", ex);
            }
        }
    }



    @Override
    public void onLocationChanged(Location location) {
        String mLastLocation = String.format(
                Locale.ENGLISH,
                "%f_%f",
                location.getLatitude(),
                location.getLongitude());

        preferences.edit()
                .putString(ADAD_LOCATION, mLastLocation)
                .apply();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}