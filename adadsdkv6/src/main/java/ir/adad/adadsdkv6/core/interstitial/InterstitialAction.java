package ir.adad.adadsdkv6.core.interstitial;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import ir.adad.adadsdkv6.models.interstitial.InterstitialModel;
import ir.adad.adadsdkv6.utils.ADAD;
import ir.adad.adadsdkv6.utils.AdService;
import ir.adad.adadsdkv6.utils.AdType;
import ir.adad.adadsdkv6.utils.Constants;
import ir.adad.adadsdkv6.utils.PrepareUrl;

public class InterstitialAction {

    public InterstitialAction(Context applicationContext, String state, InterstitialModel interstitialModel, LocalBroadcastManager localBroadcastManager) {
        action(applicationContext, state, interstitialModel, localBroadcastManager);
    }

    private void action(Context context, String state, InterstitialModel interstitialModel, LocalBroadcastManager localBroadcastManager) {

        switch (state) {
            case Constants.EVENT_AD_PLAYED: {
                localBroadcastManager
                        .sendBroadcast(new Intent(Constants.INTERSTITIAL_AD)
                                .putExtra(Constants.INTENT_EXTRA_STATE, Constants.EVENT_AD_PLAYED));

                if (ADAD.testMode != 1) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.INTENT_ADAD_CHECK, true);
                    bundle.putSerializable(Constants.INTENT_EXTRA_TYPE, AdType.INTERSTITIAL);
                    bundle.putString(Constants.INTENT_EXTRA_STATE, Constants.EVENT_AD_PLAYED);
                    bundle.putParcelable(Constants.INTENT_EXTRA_MODEL, interstitialModel);
                    context.startService(new Intent(context, AdService.class).putExtras(bundle));
                }
                break;
            }
            case Constants.EVENT_AD_CLICK: {
                localBroadcastManager
                        .sendBroadcast(new Intent(Constants.INTERSTITIAL_AD)
                                .putExtra(Constants.INTENT_EXTRA_STATE, Constants.EVENT_AD_CLICK));
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(interstitialModel.getHitUrl() +
                        PrepareUrl.getInstance(context)
                                .url("", Constants.TYPE_INTERSTITIAL, String.valueOf(interstitialModel.getAdId()))));
                context.startActivity(browserIntent);
                break;
            }
            case Constants.EVENT_AD_CLOSED: {
                localBroadcastManager
                        .sendBroadcast(new Intent(Constants.INTERSTITIAL_AD)
                                .putExtra(Constants.INTENT_EXTRA_STATE, Constants.EVENT_AD_CLOSED));
                break;
            }
            case Constants.EVENT_ACTIVITY_STOPPED: {
                localBroadcastManager
                        .sendBroadcast(new Intent(Constants.INTERSTITIAL_AD)
                                .putExtra(Constants.INTENT_EXTRA_STATE, Constants.EVENT_ACTIVITY_STOPPED));
                break;
            }
        }
    }
}
