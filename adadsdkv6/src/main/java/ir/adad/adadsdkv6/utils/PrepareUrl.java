package ir.adad.adadsdkv6.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.util.ArrayMap;
import android.telephony.TelephonyManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.zip.GZIPOutputStream;


import ir.adad.adadsdkv6.utils.Volley.VolleySingleton;

import static android.provider.Settings.Secure.ANDROID_ID;
import static ir.adad.adadsdkv6.utils.Constants.ADAD_LOCATION;
import static ir.adad.adadsdkv6.utils.Constants.BAZAAR_DEFAULT_ID_URI;
import static ir.adad.adadsdkv6.utils.Constants.BAZAAR_DEFAULT_LOCATION_URI;

public class PrepareUrl {

    private Context mContext;

    private PrepareUrl(Context context) {

        mContext = context;

        if (!Util.isMyServiceRunning(MyLocationService.class,context)){

            context.startService(new Intent(context,MyLocationService.class));

        }
    }

    public static PrepareUrl getInstance(Context context) {

        return new PrepareUrl(context);
    }

    public String url(String base, String addType, String adId) {

        ArrayMap<String, String> arrayMap = new ArrayMap<>();
        arrayMap.put("digest", digest(getAndroidId() + getAndroidId() + adId));
        arrayMap.put("device", Build.DEVICE);
        arrayMap.put("androidid", getAndroidId());
        arrayMap.put("token", ADAD.AdadToken);
        arrayMap.put("network", getNetworkClass());
        arrayMap.put("data", getDataConnectivity());
        arrayMap.put("bazaarversion", String.valueOf(getBazarVersion(mContext, "com.farsitel.bazaar")));
        arrayMap.put("deviceid", getAndroidId());
        arrayMap.put("l", String.valueOf(UniqueIdMethods.getInstance(mContext).getBazaarLoginStatus(BAZAAR_DEFAULT_ID_URI)));
        arrayMap.put("car", getNetworkOperator());
        arrayMap.put("j", getDeviceLocation());
        arrayMap.put("brand", Build.BRAND);
        arrayMap.put("package", getPackageName());
        arrayMap.put("test", String.valueOf(ADAD.testMode));
        arrayMap.put("version", "1");
        arrayMap.put("time", getTime());
        arrayMap.put("lang", getLanguage());
        arrayMap.put("android", getNetworkOperator());
        arrayMap.put("adadversion", "6");
        arrayMap.put("uid", UniqueIdMethods.getInstance(mContext).deviceUniqueId(BAZAAR_DEFAULT_ID_URI));
        arrayMap.put("model", Build.MODEL);
        arrayMap.put("library", "android");
        arrayMap.put("dpi", String.valueOf(getScrWidth()));
        arrayMap.put("orientation", String.valueOf(getOrientation()));
        arrayMap.put("cbUserId", ADAD.userId);
        arrayMap.put("adType", addType);
        arrayMap.put("adid", String.valueOf(adId));

        return base + createUrl(arrayMap);

    }

    private String createUrl(ArrayMap<String, String> arrayMap) {
        StringBuilder url = new StringBuilder("?");
        for (String key : arrayMap.keySet()) {
            url.append(String.format("%s=%s&", key, arrayMap.get(key)));
        }
        return url.toString();
    }


    private static String digest(String input) {
        MessageDigest md5;


        try {
            byte[] saltBytes = new byte[2];
            (new Random()).nextBytes(saltBytes);
            String salt = toHex(saltBytes);
            md5 = MessageDigest.getInstance("MD5");
            md5.update(input.getBytes());
            md5.update(salt.getBytes());
            byte[] digest = md5.digest();
            byte[] hash = new byte[saltBytes.length + digest.length];
            System.arraycopy(saltBytes, 0, hash, 0, saltBytes.length);
            System.arraycopy(digest, 0, hash, saltBytes.length, digest.length);
            return toHex(hash);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    private static String toHex(byte[] input) {
        String hex = new BigInteger(1, input).toString(16);
        while (hex.length() < 2 * input.length) {
            hex = "0" + hex;
        }
        return hex;
    }


    private String getAndroidId() {
        return Secure.getString(mContext.getContentResolver(),
                ANDROID_ID);
    }

    private String getTime() {
        return Double.toString(java.lang.System.currentTimeMillis());
    }

    private String getPackageName() {
        return mContext.getPackageName();
    }


    private Integer getScrWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    private Integer getScrHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    private Integer getOrientation() {

        return Resources.getSystem().getConfiguration().orientation;
    }

    private String getNetworkOperator() {

        return ((TelephonyManager) mContext
                .getSystemService(Context.TELEPHONY_SERVICE))
                .getNetworkOperator();

    }

    private String getDataConnectivity() {

        return ((TelephonyManager) mContext
                .getSystemService(Context.TELEPHONY_SERVICE))
                .getDataState() == TelephonyManager.DATA_CONNECTED ? "1" : "0";

    }


    private String getLanguage() {
        return Locale.getDefault().getLanguage();
    }

    private String getNetworkClass() {
        try {
            TelephonyManager mTelephonyManager = (TelephonyManager)
                    mContext.getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = mTelephonyManager.getNetworkType();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "4G";
                default:
                    return "";
            }
        } catch (Exception e) {
            return "";
        }
    }


    private Integer getBazarVersion(Context context, String packageName) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return null;
        }
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    packageName, 0);
            if (packageInfo == null) {
                return null;
            }
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // If here, package is not installed
            return null;
        }
    }


    public void onStop() {
    }

    public String getAppInstalled() {

        final PackageManager pm = mContext.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        JSONArray jsonArray = new JSONArray();
        for (ApplicationInfo packageInfo : packages) {
            if (!packageInfo.packageName.startsWith("com.google") && !packageInfo.packageName.startsWith("com.android")) {
                jsonArray.put(packageInfo.packageName);
            }
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("androidId", getAndroidId());
            jsonObject.put("packages", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public byte[] compress(String data) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length());
        GZIPOutputStream gzip = new GZIPOutputStream(bos);
        gzip.write(data.getBytes());
        gzip.close();
        byte[] compressed = bos.toByteArray();
        bos.close();
        return compressed;
    }


    public void sendPackagesInstalled() throws IOException {


        byte[] mahyar = compress(getAppInstalled());

        VolleySingleton.getInstance(mContext)
                .addToRequestQueue(
                        new StringRequest(Request.Method.POST,
                                "http://10.0.10.196:9000",
                                null, null) {

                            @Override
                            public byte[] getBody() throws AuthFailureError {
                                return mahyar;
                            }

                            @Override
                            public String getBodyContentType() {
                                return "application/json; charset=utf-8";
                            }
                        });

    }

    public String getDeviceLocation() {

        String deviceLocation = PreferenceManager.getDefaultSharedPreferences(mContext).getString(ADAD_LOCATION, null);
        return deviceLocation != null ? deviceLocation : "";
    }

}
