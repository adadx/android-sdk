package ir.adad.adadsdkv6.models.interstitial;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import ir.adad.adadsdkv6.models.BaseJsonModel;

/**
 * Created by deeas on 11/13/17.
 */

public class InterstitialModel extends BaseJsonModel {

    private int hideAnimationDuration ;
    private int showAnimation ;
    private String backgroundColor ;
    private String impUrl ;
    private int hideAnimation ;
    private int showAnimationDuration;
    private int orientation ;
    private String imgUrl ;
    private int opacity ;
    private String hitUrl ;


    public InterstitialModel(JSONObject jsonObject) throws JSONException {
        super(jsonObject);

       this. hideAnimationDuration = jsonObject.getInt("hideAnimationDuration");
       this. showAnimation = jsonObject.getInt("showAnimation");
       this. backgroundColor = jsonObject.getString("backgroundColor");
       this. impUrl = jsonObject.getString("impUrl");
       this. hideAnimation = jsonObject.getInt("hideAnimation");
       this. showAnimationDuration = jsonObject.getInt("showAnimationDuration");
       this. orientation = jsonObject.getInt("orientation");
       this. imgUrl = jsonObject.getString("imgUrl");
       this. opacity = jsonObject.getInt("opacity");
       this. hitUrl = jsonObject.getString("hitUrl");
    }

    public int getHideAnimationDuration() {
        return hideAnimationDuration;
    }

    public int getShowAnimation() {
        return showAnimation;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public String getImpUrl() {
        return impUrl;
    }

    public int getHideAnimation() {
        return hideAnimation;
    }

    public int getShowAnimationDuration() {
        return showAnimationDuration;
    }

    public int getOrientation() {
        return orientation;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public int getOpacity() {
        return opacity;
    }

    public String getHitUrl() {
        return hitUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.hideAnimationDuration);
        dest.writeInt(this.showAnimation);
        dest.writeString(this.backgroundColor);
        dest.writeString(this.impUrl);
        dest.writeInt(this.hideAnimation);
        dest.writeInt(this.showAnimationDuration);
        dest.writeInt(this.orientation);
        dest.writeString(this.imgUrl);
        dest.writeInt(this.opacity);
        dest.writeString(this.hitUrl);
    }

    protected InterstitialModel(Parcel in) {
        super(in);
        this.hideAnimationDuration = in.readInt();
        this.showAnimation = in.readInt();
        this.backgroundColor = in.readString();
        this.impUrl = in.readString();
        this.hideAnimation = in.readInt();
        this.showAnimationDuration = in.readInt();
        this.orientation = in.readInt();
        this.imgUrl = in.readString();
        this.opacity = in.readInt();
        this.hitUrl = in.readString();
    }

    public static final Creator<InterstitialModel> CREATOR = new Creator<InterstitialModel>() {
        @Override
        public InterstitialModel createFromParcel(Parcel source) {
            return new InterstitialModel(source);
        }

        @Override
        public InterstitialModel[] newArray(int size) {
            return new InterstitialModel[size];
        }
    };
}
