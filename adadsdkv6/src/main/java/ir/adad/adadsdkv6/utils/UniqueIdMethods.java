package ir.adad.adadsdkv6.utils;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import android.provider.Settings;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import static android.provider.Settings.Secure.ANDROID_ID;

class UniqueIdMethods {
    static private UniqueIdMethods mInstance;

    private String mGoogleAdvertisingId = null;
    private String mBazaarAdvertisingId = null;
    private String mBazaarLoginStatus = "0";
    private Context mContext;

    private UniqueIdMethods(Context context) {
        mContext = context.getApplicationContext();
    }

    private static GoogleAdInfo getGoogleAdvertisingIdInfo(Context context) throws Exception {
        if(Looper.myLooper() == Looper.getMainLooper()) throw new IllegalStateException("Cannot be called from the main thread");

        try { PackageManager pm = context.getPackageManager(); pm.getPackageInfo("com.android.vending", 0); }
        catch (Exception e) { throw e; }

        AdvertisingConnection connection = new AdvertisingConnection();
        Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
        intent.setPackage("com.google.android.gms");
        if(context.bindService(intent, connection, Context.BIND_AUTO_CREATE)) {
            try {
                AdvertisingInterface adInterface = new AdvertisingInterface(connection.getBinder());
                GoogleAdInfo adInfo = new GoogleAdInfo(adInterface.getId(), adInterface.isLimitAdTrackingEnabled(true));
                return adInfo;
            } catch (Exception exception) {
                throw exception;
            } finally {
                context.unbindService(connection);
            }
        }
        throw new IOException("Google Play connection failed");
    }

    static UniqueIdMethods getInstance(Context context) {
        if (mInstance == null)
            mInstance = new UniqueIdMethods(context);

        return mInstance;
    }

    private void updateGoogleAdvertisingId() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        GoogleAdInfo adInfo = getGoogleAdvertisingIdInfo(mContext);
                        mGoogleAdvertisingId = adInfo.getId();
                    } catch (Exception e) {
                    }
                }
            }).start();
    }

    private void updateBazaarAdvertisingId(String uri) {
        ContentResolver contentResolver = mContext.getContentResolver();
        Uri bazaarUidUri = Uri.parse(uri);
        Cursor cursor = contentResolver.query(bazaarUidUri, null, null, null,
                null);
        try {
            if (cursor != null && cursor.moveToFirst()
                    && cursor.getColumnCount() > 2) {
                mBazaarAdvertisingId = cursor.getString(1);
                mBazaarLoginStatus = cursor.getString(2);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    String getBazaarLoginStatus(String bazaarUri) {
        if (mBazaarLoginStatus == null)
            updateBazaarAdvertisingId(bazaarUri);

        return mBazaarLoginStatus;
    }

    String deviceUniqueId(String bazaarUri) {
        String deviceUid = null;
        String prefix = null;

        if (mGoogleAdvertisingId == null)
            updateGoogleAdvertisingId();

        if (mBazaarAdvertisingId == null)
            updateBazaarAdvertisingId(bazaarUri);

        if (mGoogleAdvertisingId != null) {
            prefix = "A";
            deviceUid = mGoogleAdvertisingId;
        } else if (mBazaarAdvertisingId != null) {
            prefix = "B";
            deviceUid = mBazaarAdvertisingId;
        } else {
            prefix = "C";
            deviceUid = getAndroidId();
        }
        return prefix + deviceUid;
    }

    private String getAndroidId() {
        return Settings.Secure.getString(mContext.getContentResolver(),
                ANDROID_ID);
    }

    void updateUniqueId(String bazaarUri) {
        updateGoogleAdvertisingId();
        updateBazaarAdvertisingId(bazaarUri);
    }

    String getUniqueIdFromGooglePlayServices() {
        if (mGoogleAdvertisingId != null)
            updateGoogleAdvertisingId();
        return mGoogleAdvertisingId;
    }

    String getUniqueIdFromBazaar(String uri) {
        if (mBazaarAdvertisingId != null)
            updateBazaarAdvertisingId(uri);
        return mBazaarAdvertisingId;
    }

    private static final class GoogleAdInfo {
        private final String advertisingId;
        private final boolean limitAdTrackingEnabled;

        GoogleAdInfo(String advertisingId, boolean limitAdTrackingEnabled) {
            this.advertisingId = advertisingId;
            this.limitAdTrackingEnabled = limitAdTrackingEnabled;
        }

        public String getId() {
            return this.advertisingId;
        }

        public boolean isLimitAdTrackingEnabled() {
            return this.limitAdTrackingEnabled;
        }
    }

    private static final class AdvertisingConnection implements ServiceConnection {
        private final LinkedBlockingQueue<IBinder> queue = new LinkedBlockingQueue<IBinder>(1);
        boolean retrieved = false;

        public void onServiceConnected(ComponentName name, IBinder service) {
            try { this.queue.put(service); }
            catch (InterruptedException localInterruptedException){}
        }

        public void onServiceDisconnected(ComponentName name){}

        public IBinder getBinder() throws InterruptedException {
            if (this.retrieved) throw new IllegalStateException();
            this.retrieved = true;
            return (IBinder)this.queue.take();
        }
    }

    private static final class AdvertisingInterface implements IInterface {
        private IBinder binder;

        public AdvertisingInterface(IBinder pBinder) {
            binder = pBinder;
        }

        public IBinder asBinder() {
            return binder;
        }

        public String getId() throws RemoteException {
            Parcel data = Parcel.obtain();
            Parcel reply = Parcel.obtain();
            String id;
            try {
                data.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                binder.transact(1, data, reply, 0);
                reply.readException();
                id = reply.readString();
            } finally {
                reply.recycle();
                data.recycle();
            }
            return id;
        }

        public boolean isLimitAdTrackingEnabled(boolean paramBoolean) throws RemoteException {
            Parcel data = Parcel.obtain();
            Parcel reply = Parcel.obtain();
            boolean limitAdTracking;
            try {
                data.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                data.writeInt(paramBoolean ? 1 : 0);
                binder.transact(2, data, reply, 0);
                reply.readException();
                limitAdTracking = 0 != reply.readInt();
            } finally {
                reply.recycle();
                data.recycle();
            }
            return limitAdTracking;
        }
    }
}
