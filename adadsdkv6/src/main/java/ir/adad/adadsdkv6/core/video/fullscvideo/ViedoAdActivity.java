package ir.adad.adadsdkv6.core.video.fullscvideo;

import android.content.Context;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;

import ir.adad.adadsdkv6.R;
import ir.adad.adadsdkv6.models.videoJson.VideoModel;

import ir.adad.adadsdkv6.utils.AdType;
import ir.adad.adadsdkv6.utils.Constants;
import ir.adad.adadsdkv6.utils.Util;


public class ViedoAdActivity extends AppCompatActivity {

    private RelativeLayout richConstraintLayout;
    private VideoView videoView;
    private ProgressBar mProgressBar;
    private ImageView volumeImageView;
    private ImageView bannerImageView;
    private ImageView logoImageView;
    private TextView titleTextView;
    private ImageView skipImageView;
    private ImageView replayImageView;
    private ImageView adadWaterMark;
    private ImageView buttonImageView;
    private ContentObserver audioVolumeContentObserver;
    private boolean isVolumeMuted;
    private boolean isFirstTimeCompleted = true;
    private AudioManager audioManager;
    private LocalBroadcastManager localBroadcastManager;
    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_full_sc);
        init();

    }

    private void init() {

        VideoModel videoModel = getIntent().getParcelableExtra(Constants.INTENT_EXTRA_MODEL);
        AdType adType = (AdType) getIntent().getSerializableExtra(Constants.INTENT_EXTRA_TYPE);

        richConstraintLayout = (RelativeLayout) findViewById(R.id.rich_view_constraintLayout);
        skipImageView = (ImageView) findViewById(R.id.skip_imageView);
        adadWaterMark = (ImageView) findViewById(R.id.adad_waterMark);

        //richConstraintLayout
        videoView = (VideoView) findViewById(R.id.videoView);
        mProgressBar = (ProgressBar) findViewById(R.id.Progressbar);
        mProgressBar.setProgress(0);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        volumeImageView = (ImageView) findViewById(R.id.volume_imageView);

        //richWrapper
        bannerImageView = (ImageView) findViewById(R.id.rich_banner_imageView);
        replayImageView = (ImageView) findViewById(R.id.rich_replay_imageView);
        logoImageView = (ImageView) findViewById(R.id.rich_logo_imageView);
        titleTextView = (TextView) findViewById(R.id.rich_title_textView);
        buttonImageView = (ImageView) findViewById(R.id.rich_button_imageView);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                updateVideoView();
            }
        };

        onAd(videoModel, adType);

    }

    private void onAd(final VideoModel videoModel, final AdType adType) {
        show(videoModel, adType);
        checkVolume();

        skipImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (adType == AdType.REWARDEDVIDEO && isFirstTimeCompleted) {

                    dialogView(videoView.getCurrentPosition(), videoModel, adType);

                } else {

                    if (isFirstTimeCompleted) {
                        skippedState(videoView.getCurrentPosition(), videoModel, adType);
                    } else {
                        closedState();
                    }
                }
            }
        });

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isVolumeMuted) {
                    UnMuteAudio();
                }
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                completedState(videoModel, adType);
            }
        });

        volumeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isVolumeMuted) {
                    UnMuteAudio();
                } else {
                    MuteAudio();
                }
            }
        });

        audioVolumeContentObserver = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                if (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0) {
                    volumeImageView.setImageResource(R.drawable.video_ic_mute);
                } else {
                    volumeImageView.setImageResource(R.drawable.video_ic_unmute);
                }
            }
        };

        richConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adClickedState(videoModel);
            }
        });

        registerContentObserver();
    }

    public void show(final VideoModel videoModel, final AdType adType) {
        prepareViewForVideo(videoModel);
        videoView.setVideoURI(Uri.parse(videoModel.getVideo()));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                videoView.start();
                videoShowState(videoModel, adType);
            }
        });

        updateVideoView();
    }

    private void updateVideoView() {

        int videoProgress = videoView.getCurrentPosition() * 100 / videoView.getDuration();
        mProgressBar.setProgress(videoProgress);
        if (videoProgress >= 5 && skipImageView.getVisibility() == View.GONE) {
            skipImageView.setVisibility(View.VISIBLE);
        }
        handler.postDelayed(runnable, 100);
    }

    private void videoShowState(VideoModel videoModel, AdType adType) {
        action(this, Constants.EVENT_AD_PLAYED, adType, videoModel, isFirstTimeCompleted, null, null);
    }

    private void skippedState(int skipTime, VideoModel videoModel, AdType adType) {
        action(this, Constants.EVENT_SKIPPED, adType, videoModel, isFirstTimeCompleted, localBroadcastManager, skipTime);
        finish();
    }

    private void closedState() {
        action(this, Constants.EVENT_AD_CLOSED, null, null, null, localBroadcastManager, null);
        finish();
    }

    private void adClickedState(VideoModel videoModel) {
        action(this, Constants.EVENT_AD_CLICK, null, videoModel, null, localBroadcastManager, null);
        finish();
    }

    private void completedState(VideoModel videoModel, AdType adType) {
        action(this, Constants.EVENT_COMPLETED, adType, videoModel, isFirstTimeCompleted, localBroadcastManager, null);
        if (videoModel.hasRich()) {
            handler.removeCallbacks(runnable);
            prepareViewForRich(videoModel, adType);
        } else {
            finish();
        }
        replayImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  hideSystemUI();
                prepareViewForVideo(videoModel);
                videoView.start();
                updateVideoView();
            }
        });
        isFirstTimeCompleted = false;
    }

    private void dialogView(final int videoSkipSec, final VideoModel videoModel, final AdType adType) {
        videoView.pause();
        new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar)
                .setMessage("در این صورت به شما جایزه ای نمیدیم")
                .setCancelable(true)
                .setPositiveButton(
                        "باشه",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                skippedState(videoView.getCurrentPosition(), videoModel, adType);
                            }
                        })
                .setNegativeButton("ادامه بده", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        hideSystemUI();
                        videoView.start();
                    }
                })
                .create()
                .show();
    }

    private void checkVolume() {

        if (audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM) == 0) {
            isVolumeMuted = true;
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
            volumeImageView.setImageResource(R.drawable.video_ic_mute);
        } else {
            isVolumeMuted = false;
            volumeImageView.setImageResource(R.drawable.video_ic_unmute);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC
                    , audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM)
                    , 0);
        }
    }

    public void MuteAudio() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
        } else {
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
        }

        volumeImageView.setImageResource(R.drawable.video_ic_mute);
        isVolumeMuted = true;


    }

    public void UnMuteAudio() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, 0);
        } else {
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        }

        volumeImageView.setImageResource(R.drawable.video_ic_unmute);
        isVolumeMuted = false;
    }

    private void unregisterContentObserver() {
        if (audioVolumeContentObserver != null) {
            getContentResolver().unregisterContentObserver(audioVolumeContentObserver);
        }
    }

    private void registerContentObserver() {
        getContentResolver().registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, audioVolumeContentObserver);
    }

    private void prepareViewForVideo(VideoModel videoModel) {

        Glide.with(this)
                .load(videoModel.getLogoAdad())
                .into(adadWaterMark);

        adadWaterMark.setVisibility(View.VISIBLE);
        skipImageView.setVisibility(View.VISIBLE);
        videoView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        volumeImageView.setVisibility(View.VISIBLE);

        richConstraintLayout.setVisibility(View.GONE);
        bannerImageView.setVisibility(View.GONE);
        logoImageView.setVisibility(View.GONE);
        titleTextView.setVisibility(View.GONE);
        replayImageView.setVisibility(View.GONE);
        buttonImageView.setVisibility(View.GONE);

    }

    private void prepareViewForRich(VideoModel videoModel, AdType adType) {

        Glide.with(this).load(videoModel.getImageUrl()).into(bannerImageView);
        Glide.with(this).load(videoModel.getLogo()).into(logoImageView);
        Glide.with(this).load(videoModel.getButtonIcon()).into(buttonImageView);

        titleTextView.setText(videoModel.getText());
        titleTextView.setTextColor(Color.parseColor(videoModel.getRichTextColor()));
        richConstraintLayout.setBackgroundColor(Color.parseColor(videoModel.getRichBottomBarColor()));

        videoView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        volumeImageView.setVisibility(View.GONE);


        richConstraintLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            boolean oneTime = true;

            @Override
            public void onGlobalLayout() {

                if (richConstraintLayout.getHeight() != 0 && oneTime) {
                    oneTime = false;


                    RelativeLayout.LayoutParams logoLayoutParams = (RelativeLayout.LayoutParams) logoImageView.getLayoutParams();
                    logoLayoutParams.height = (int) (richConstraintLayout.getHeight() * 0.4) ;
                    logoLayoutParams.width = (int) (richConstraintLayout.getHeight() * 0.4) ;

                    RelativeLayout.LayoutParams buttonLayoutParams = (RelativeLayout.LayoutParams) buttonImageView.getLayoutParams();
                    buttonLayoutParams.height = (int) (richConstraintLayout.getHeight() * 0.4) ;
                    buttonLayoutParams.width = (int) (richConstraintLayout.getHeight() * 0.4) ;

                }
            }
        });

        RelativeLayout.LayoutParams bannerImageViewLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (Util.getScreenWidth() * 0.5));
        bannerImageView.setLayoutParams(bannerImageViewLayoutParams);

        adadWaterMark.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            boolean oneTime = true;

            @Override
            public void onGlobalLayout() {

                if (adadWaterMark.getWidth() != 0 && oneTime) {
                    oneTime = false;

                    RelativeLayout.LayoutParams buttonImageViewLayoutParams = (RelativeLayout.LayoutParams) buttonImageView.getLayoutParams();
                    buttonImageViewLayoutParams.rightMargin = adadWaterMark.getWidth() + ((RelativeLayout.LayoutParams) adadWaterMark.getLayoutParams()).rightMargin + 50;

                }
            }
        });


        adadWaterMark.setVisibility(View.VISIBLE);
        bannerImageView.setVisibility(View.VISIBLE);
        richConstraintLayout.setVisibility(View.VISIBLE);
        logoImageView.setVisibility(View.VISIBLE);
        buttonImageView.setVisibility(View.VISIBLE);
        titleTextView.setVisibility(View.VISIBLE);
        replayImageView.setVisibility(View.VISIBLE);

        action(this, Constants.EVENT_VIDEO_RICH_VIEW, adType, videoModel, isFirstTimeCompleted, null, null);
    }

    private void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideSystemUI();
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
        videoView.stopPlayback();
        unregisterContentObserver();
        action(this, Constants.EVENT_ACTIVITY_STOPPED, null, null, null, localBroadcastManager, null);
    }

    private void action(Context context, String state, AdType adType, VideoModel videoModel, Boolean isFirstTimeCompleted, LocalBroadcastManager localBroadcastManager, Integer skipTime) {
        new VideoAdAction(context, state, adType, videoModel, isFirstTimeCompleted, localBroadcastManager, skipTime);
    }


}
