package ir.adad.adadsdkv6.core.video;

public interface VideoAdListener {

	public void onAdFinished(boolean wasSuccessfulView, boolean wasCallToActionClicked , int skipTime);
	public void onAdStarted();
	public void onAdFailedToLoad(int errorNumber);
}
