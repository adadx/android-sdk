package ir.adad.adadsdkv6.core.interstitial;

public interface InterstitialAdListener {

    public void onAdOpened();
    public void onAdClosed();
    public void onAdClicked();
    public void onAdFailedToLoad(int errorNumber);
    public void onAdLoaded(InterstitialAd interstitialAd);

}
