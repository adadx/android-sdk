package ir.adad.adadsdkv6.utils;

import android.content.Context;
import android.os.Bundle;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import ir.adad.adadsdkv6.models.bannerJson.BannerModel;
import ir.adad.adadsdkv6.models.bannerJson.TextModel;
import ir.adad.adadsdkv6.models.interstitial.InterstitialModel;
import ir.adad.adadsdkv6.models.videoJson.VideoModel;
import ir.adad.adadsdkv6.utils.Volley.VolleySingleton;

import static ir.adad.adadsdkv6.utils.Constants.INTENT_ADAD_CHECK;
import static ir.adad.adadsdkv6.utils.Constants.TYPE_BANNER;
import static ir.adad.adadsdkv6.utils.Constants.TYPE_INTERSTITIAL;

public class AdServiceRequest {


    private Context mContext;

    public AdServiceRequest(Context context, Bundle bundle) {

        mContext = context;
        sendRequest(bundle);

    }

    private void sendRequest(Bundle intent) {

        if (intent.getBoolean(INTENT_ADAD_CHECK, false)) {

            AdType adType = (AdType) intent.getSerializable(Constants.INTENT_EXTRA_TYPE);

            String state = intent.getString(Constants.INTENT_EXTRA_STATE);

            if (adType == AdType.REWARDEDVIDEO || adType == AdType.FULLSCVIDEO) {

                int skipSec = intent.getInt(Constants.INTENT_EXTRA_SKIP_TIME, -1);
                VideoModel videoModel = intent.getParcelable(Constants.INTENT_EXTRA_MODEL);
                switch (state) {
                    case Constants.EVENT_SKIPPED:

                        netRequest(videoModel.getVideoPlayedUrl() +
                                PrepareUrl.getInstance(mContext)
                                        .url("", videoModel.getContentType(), String.valueOf(videoModel.getAdId())) +
                                "&skippedtime=" + skipSec);
                        break;
                    case Constants.EVENT_AD_PLAYED:
                        netRequest(videoModel.getVideoCheckUrl() +
                                PrepareUrl.getInstance(mContext)
                                        .url("", videoModel.getContentType(), String.valueOf(videoModel.getAdId())));
                        break;
                    case Constants.EVENT_COMPLETED:
                        netRequest(videoModel.getVideoPlayedUrl() +
                                PrepareUrl.getInstance(mContext)
                                        .url("", videoModel.getContentType(), String.valueOf(videoModel.getAdId())));
                        break;
                    case Constants.EVENT_VIDEO_RICH_VIEW:
                        netRequest(videoModel.getImpUrl() +
                                PrepareUrl.getInstance(mContext)
                                        .url("", videoModel.getContentType(), String.valueOf(videoModel.getAdId())));
                        break;
                    case Constants.EVENT_AD_CLICK:
                        netRequest(videoModel.getHitUrl() +
                                PrepareUrl.getInstance(mContext)
                                        .url("", videoModel.getContentType(), String.valueOf(videoModel.getAdId())));
                        break;
                }
            } else if (adType == AdType.IMAGEBANNER) {

                BannerModel bannerModel = intent.getParcelable(Constants.INTENT_EXTRA_MODEL);

                switch (state) {
                    case Constants.EVENT_AD_PLAYED:
                        netRequest(bannerModel.getImpUrl() +
                                PrepareUrl.getInstance(mContext)
                                        .url("", TYPE_BANNER, String.valueOf(bannerModel.getAdId())));
                        break;

                    case Constants.EVENT_API_AD_PLAYED:
                        VolleySingleton.getInstance(mContext).addToRequestQueue(new Request<Object>(Request.Method.GET, bannerModel.getPixelUrl(), new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                debug("error : " + error.toString());
                            }
                        }) {
                            @Override
                            protected Response<Object> parseNetworkResponse(NetworkResponse response) {
                                debug("response : " + response.statusCode);
                                return null;
                            }

                            @Override
                            protected void deliverResponse(Object response) {
                            }
                        });
                        break;
                }
            } else if (adType == AdType.TEXTBANNER) {

                TextModel textModel = intent.getParcelable(Constants.INTENT_EXTRA_MODEL);

                switch (state) {

                    case Constants.EVENT_AD_PLAYED:
                        netRequest(textModel.getImpUrl() +
                                PrepareUrl.getInstance(mContext)
                                        .url("", TYPE_BANNER, String.valueOf(textModel.getAdId())));
                        break;
                }
            } else if (adType == AdType.INTERSTITIAL) {

                InterstitialModel interstitialModel = intent.getParcelable(Constants.INTENT_EXTRA_MODEL);

                switch (state) {

                    case Constants.EVENT_AD_PLAYED:
                        netRequest(interstitialModel.getImpUrl() +
                                PrepareUrl.getInstance(mContext)
                                        .url("", TYPE_INTERSTITIAL, String.valueOf(interstitialModel.getAdId())));
                        break;
                }
            }
        }


    }

    private void netRequest(String url) {


        VolleySingleton.getInstance(mContext).addToRequestQueue(new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                debug("response : " + response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                debug("error : " + error.toString());

            }
        }));

    }

    private void debug(String msg) {

        /*if (BuildConfig.DEBUG) {
            Log.d("Service", "debug: " + msg);
        }*/
    }
}
