package ir.adad.adadsdksample;

import android.Manifest;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import ir.adad.adadsdkv6.core.banner.BannerAdListener;
import ir.adad.adadsdkv6.core.banner.views.BannerAdView;
import ir.adad.adadsdkv6.core.interstitial.InterstitialAd;
import ir.adad.adadsdkv6.core.interstitial.InterstitialAdListener;
import ir.adad.adadsdkv6.core.video.VideoAdFactory;
import ir.adad.adadsdkv6.utils.AdType;
import ir.adad.adadsdkv6.core.video.fullscvideo.VideoAd;
import ir.adad.adadsdkv6.core.video.VideoAdListener;
import ir.adad.adadsdkv6.utils.ADAD;
import ir.adad.adadsdkv6.utils.PermissionHandler;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private VideoAd fullScVideoAd;


    BannerAdView bannerAdView;
    InterstitialAd interstitialAd;
    VideoAd rewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ADAD.init("0860b78da7e44b0da01ff1611ce0ce8e");
        ADAD.setTestMode(0);

        setContentView(R.layout.activity_main);

        new PermissionHandler().checkPermission(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                new PermissionHandler.OnPermissionResponse() {
                    @Override
                    public void onPermissionGranted() {

                    }

                    @Override
                    public void onPermissionDenied() {

                    }
                });


        bannerAdView = (BannerAdView) findViewById(R.id.bannerAdView);
        bannerAdView
                .setUserId("")
                .setBannerAdListener(new BannerAdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorNumber) {

                        Log.i(TAG, "(ADAD) bannerAdView --> onAdFailedToLoad : " + " errorNumber =" + errorNumber);

                    }

                    @Override
                    public void onAdShow() {
                        Log.i(TAG, "(ADAD) bannerAdView --> onAdShow: ");
                    }

                    @Override
                    public void onAdClosed() {
                        Log.i(TAG, "(ADAD) bannerAdView --> onAdClosed: ");
                    }

                    @Override
                    public void onAdAvailable() {
                        Log.i(TAG, "(ADAD) bannerAdView --> onAdAvailable: ");
                    }

                    @Override
                    public void onAdClicked() {
                        Log.i(TAG, "(ADAD) bannerAdView --> onAdClicked: ");
                    }
                });


        interstitialAd = new InterstitialAd(MainActivity.this)
                .setUserId("")
                .setInterstitialAdListener(new InterstitialAdListener() {
                    @Override
                    public void onAdOpened() {
                        Log.i(TAG, "(ADAD) interstitialAd --> onAdOpened: ");
                    }

                    @Override
                    public void onAdClosed() {
                        Log.i(TAG, "(ADAD) interstitialAd --> onAdClosed: ");
                    }

                    @Override
                    public void onAdClicked() {
                        Log.i(TAG, "(ADAD) interstitialAd --> onAdClicked: ");
                    }

                    @Override
                    public void onAdFailedToLoad(int errorNumber) {

                        Log.i(TAG, "(ADAD) interstitialAd --> onAdFailedToLoad: " + " errorNumber = " + errorNumber);
                    }

                    @Override
                    public void onAdLoaded(InterstitialAd interstitialAd) {
                        Log.i(TAG, "(ADAD) interstitialAd --> onAdLoaded: ");
                    }
                });

        rewardedVideoAd = VideoAdFactory.getInstance(MainActivity.this)
                .setAdType(AdType.REWARDEDVIDEO)
                .setUserId("1548")
                .build();

        rewardedVideoAd.setVideoAdListener(new VideoAdListener() {
            @Override
            public void onAdFinished(boolean wasSuccessfulView, boolean wasCallToActionClicked, int skipTime) {
                Log.i(TAG, "(ADAD) rewardedVideoAd --> onAdFinished: " + "wasSuccessfulView = " + wasSuccessfulView + "wasCallToActionClicked = " + wasCallToActionClicked + "skipTime = " + skipTime);
            }

            @Override
            public void onAdStarted() {
                Log.i(TAG, "(ADAD) rewardedVideoAd --> onAdStarted: ");
            }

            @Override
            public void onAdFailedToLoad(int reason) {
                Log.i(TAG, "(ADAD) rewardedVideoAd --> onAdFailedToLoad: " + "reason = " + reason);
            }

        });

        fullScVideoAd = VideoAdFactory.getInstance(MainActivity.this)
                .setAdType(AdType.FULLSCVIDEO)
                .setUserId("")
                .build();
        fullScVideoAd
                .setVideoAdListener(new VideoAdListener() {
                    @Override
                    public void onAdFinished(boolean wasSuccessfulView, boolean wasCallToActionClicked, int skipTime) {
                        Log.i(TAG, "(ADAD) fullScVideoAd --> onAdFinished: " + "wasSuccessfulView = " + wasSuccessfulView + "wasCallToActionClicked = " + wasCallToActionClicked + "skipTime = " + skipTime);

                    }

                    @Override
                    public void onAdStarted() {
                        Log.i(TAG, "(ADAD) fullScVideoAd --> onAdStart");
                    }

                    @Override
                    public void onAdFailedToLoad(int reasonNumber) {
                        Log.i(TAG, "(ADAD) fullScVideoAd --> onAdFailedToLoad: " + "reasonNumber = " + reasonNumber);
                    }
                });

    }

    @Override
    protected void onStop() {
        if (bannerAdView != null) {
            bannerAdView.onStop();
        }
        super.onStop();
    }


    @Override
    protected void onResume() {
        if (bannerAdView != null) {
            bannerAdView.onResume();
        }
        super.onResume();
    }


    @Override
    protected void onPause() {

        if (bannerAdView != null) {
            bannerAdView.onPause();
        }
        super.onPause();
    }


    public void onBanner(View view) {

        if (bannerAdView != null) {
            bannerAdView.enableBannerAds();
        }
    }

    public void offBanner(View view) {
        if (bannerAdView != null) {
            bannerAdView.disableBannerAds();
        }

    }

    public void interstitialPlayAd(View view) {

        if (interstitialAd != null) {
            interstitialAd.showAd();
        }
    }

    public void rewardedLoadAd(View view) {
        if (rewardedVideoAd != null) {
            rewardedVideoAd.prepareAd();
        }
    }

    public void interstitialLoadAd(View view) {
        if (interstitialAd != null) {
            interstitialAd.prepareAd();
        }
    }

    public void rewardedPlayAd(View view) {
        if (rewardedVideoAd != null) {
            rewardedVideoAd.showAd();
        }
    }

    public void fullScLoadAd(View view) {
        if (fullScVideoAd != null) {
            fullScVideoAd.prepareAd();
        }
    }

    public void fullScPlayAd(View view) {
        if (fullScVideoAd != null) {
            fullScVideoAd.showAd();
        }
    }
}