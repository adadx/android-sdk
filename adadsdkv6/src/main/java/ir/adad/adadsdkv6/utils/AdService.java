package ir.adad.adadsdkv6.utils;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;



public class AdService extends IntentService {

    public AdService() {
        super("AdService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if (intent.getExtras() != null) {
            new AdServiceRequest(this, intent.getExtras());
        }
    }
}
