package ir.adad.adadsdkv6.utils;

public enum AdType {

    REWARDEDVIDEO,
    FULLSCVIDEO,
    PRELOAD,
    INTERSTITIAL,
    TEXTBANNER,
    IMAGEBANNER;

    @Override
    public String toString() {
        return super.toString();
    }
}
