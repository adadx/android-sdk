package ir.adad.adadsdkv6.core.banner;

/**
 * Created by saeed on 9/4/17.
 */

public interface BannerAdListener {

    public void onAdFailedToLoad(int errorNumber);
    public void onAdShow();
    public void onAdClosed();
    public void onAdAvailable();
    public void onAdClicked();
}
