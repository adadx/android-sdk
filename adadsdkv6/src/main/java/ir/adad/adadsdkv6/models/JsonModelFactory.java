package ir.adad.adadsdkv6.models;

import org.json.JSONException;
import org.json.JSONObject;

import ir.adad.adadsdkv6.models.bannerJson.BannerModel;
import ir.adad.adadsdkv6.models.bannerJson.TextModel;
import ir.adad.adadsdkv6.models.interstitial.InterstitialModel;
import ir.adad.adadsdkv6.models.videoJson.VideoModel;

import static ir.adad.adadsdkv6.utils.Constants.TYPE_HYBRID;
import static ir.adad.adadsdkv6.utils.Constants.TYPE_IMAGEBANNER;
import static ir.adad.adadsdkv6.utils.Constants.TYPE_TEXTBANNER;
import static ir.adad.adadsdkv6.utils.Constants.TYPE_VIDEO;

/**
 * Created by saeed on 10/29/17.
 */

public class JsonModelFactory {


    public static BaseJsonModel getInstants(JSONObject jsonObject) throws JSONException {


        switch (jsonObject.getString("contentType")) {

            case TYPE_IMAGEBANNER:

                return new BannerModel(jsonObject);

            case TYPE_TEXTBANNER:

                return new TextModel(jsonObject);

            case TYPE_VIDEO:

                return new VideoModel(jsonObject);

            case TYPE_HYBRID:

                return new VideoModel(jsonObject);

            case "interstitial-image":

                return new InterstitialModel(jsonObject);


            default:
                return null;
        }
    }
}
