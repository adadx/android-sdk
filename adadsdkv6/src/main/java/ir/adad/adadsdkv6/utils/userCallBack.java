package ir.adad.adadsdkv6.utils;


public interface userCallBack {

    void onSkipped();
    void onCompleted();
}
