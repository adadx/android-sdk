
package ir.adad.adadsdkv6.models.bannerJson;

import android.os.Parcel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ir.adad.adadsdkv6.models.BaseJsonModel;

public class TextModel extends BaseJsonModel {

    private List<String> subtitle;
    private String buttonIcon;
    private String textBackgroundColor;
    private String logo;
    private String textColor;
    private String title;
    private String hitUrl;
    private String impUrl;

    public TextModel(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        this.subtitle = convertJsonArray(jsonObject.getJSONArray("subtitle"));
        this.buttonIcon = jsonObject.getString("buttonIcon");
        this.textBackgroundColor = jsonObject.getString("textBackgroundColor");
        this.logo = jsonObject.getString("logoUrl");
        this.textColor = jsonObject.getString("textColor");
        this.title = jsonObject.getString("title");
        this.hitUrl = jsonObject.getString("hitUrl");
        this.impUrl = jsonObject.getString("impUrl");
    }

    public List<String> getSubtitle() {
        return subtitle;
    }

    public String getButtonIcon() {
        return buttonIcon;
    }

    public String getTextBackgroundColor() {
        return textBackgroundColor;
    }

    public String getLogo() {
        return logo;
    }

    public String getTextColor() {
        return textColor;
    }

    public String getTitle() {
        return title;
    }

    public String getHitUrl() {
        return hitUrl;
    }

    public String getImpUrl() {
        return impUrl;
    }




    public List<String> convertJsonArray(JSONArray jsonArray) {
        ArrayList<String> listdata = new ArrayList<String>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {

                listdata.add(jsonArray.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return listdata;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeStringList(this.subtitle);
        dest.writeString(this.buttonIcon);
        dest.writeString(this.textBackgroundColor);
        dest.writeString(this.logo);
        dest.writeString(this.textColor);
        dest.writeString(this.title);
        dest.writeString(this.hitUrl);
        dest.writeString(this.impUrl);
    }

    protected TextModel(Parcel in) {
        super(in);
        this.subtitle = in.createStringArrayList();
        this.buttonIcon = in.readString();
        this.textBackgroundColor = in.readString();
        this.logo = in.readString();
        this.textColor = in.readString();
        this.title = in.readString();
        this.hitUrl = in.readString();
        this.impUrl = in.readString();
    }

    public static final Creator<TextModel> CREATOR = new Creator<TextModel>() {
        @Override
        public TextModel createFromParcel(Parcel source) {
            return new TextModel(source);
        }

        @Override
        public TextModel[] newArray(int size) {
            return new TextModel[size];
        }
    };
}



