
package ir.adad.adadsdkv6.models.bannerJson;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import ir.adad.adadsdkv6.models.BaseJsonModel;

public class BannerModel extends BaseJsonModel implements Parcelable {


    private String imgUrl;
    private String hitUrl;
    private String impUrl;
    private String pixelUrl = "";

    public BannerModel(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        this.imgUrl = jsonObject.getString("imgUrl");
        this.impUrl = jsonObject.getString("impUrl");
        this.hitUrl = jsonObject.getString("hitUrl");

        if (!jsonObject.getString("pixelUrl").isEmpty()) {
            this.pixelUrl = jsonObject.getString("pixelUrl");
        }
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getHitUrl() {
        return hitUrl;
    }

    public String getImpUrl() {
        return impUrl;
    }

    public String getPixelUrl() {
        return pixelUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.imgUrl);
        dest.writeString(this.hitUrl);
        dest.writeString(this.impUrl);
        dest.writeString(this.pixelUrl);
    }

    protected BannerModel(Parcel in) {
        super(in);
        this.imgUrl = in.readString();
        this.hitUrl = in.readString();
        this.impUrl = in.readString();
        this.pixelUrl = in.readString();
    }

    public static final Creator<BannerModel> CREATOR = new Creator<BannerModel>() {
        @Override
        public BannerModel createFromParcel(Parcel source) {
            return new BannerModel(source);
        }

        @Override
        public BannerModel[] newArray(int size) {
            return new BannerModel[size];
        }
    };
}
