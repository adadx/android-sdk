package ir.adad.adadsdkv6.core.interstitial;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.bumptech.glide.Glide;

import java.lang.ref.WeakReference;

import ir.adad.adadsdkv6.R;
import ir.adad.adadsdkv6.core.banner.RequestAd;
import ir.adad.adadsdkv6.core.banner.RequestAdCallBack;
import ir.adad.adadsdkv6.models.BaseJsonModel;
import ir.adad.adadsdkv6.models.interstitial.InterstitialModel;
import ir.adad.adadsdkv6.utils.AdType;
import ir.adad.adadsdkv6.utils.Constants;
import ir.adad.adadsdkv6.utils.PrepareUrl;
import ir.adad.adadsdkv6.utils.Volley.VolleySingleton;

public class InterstitialAd implements RequestAdCallBack {

    private final String TAG = Constants.INTERSTITIAL_AD;
    private WeakReference<Activity> activity;
    private InterstitialAdListener userInterstitialAdListener;
    private Context context;
    private PrepareUrl prepareUrl;
    private RequestAd requestAd;
    private InterstitialModel interstitialModel;
    private boolean adLoaded = false;
    private String userId;
    private long lastMillis = 0;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getStringExtra(Constants.INTENT_EXTRA_STATE)) {
                case Constants.EVENT_AD_CLOSED:
                    if (userInterstitialAdListener != null) {
                        userInterstitialAdListener.onAdClosed();
                    }
                    break;
                case Constants.EVENT_ACTIVITY_STOPPED:

                    LocalBroadcastManager.getInstance(context).unregisterReceiver(this);

                    if (prepareUrl != null) {
                        prepareUrl.onStop();
                    }

                    if (requestAd != null) {
                        requestAd.onCancel();
                    }

                    break;
                case Constants.EVENT_AD_CLICK:
                    if (userInterstitialAdListener != null) {
                        userInterstitialAdListener.onAdClicked();
                    }
                    break;

                case Constants.EVENT_AD_PLAYED:
                    if (userInterstitialAdListener != null) {
                        userInterstitialAdListener.onAdOpened();
                    }
                    break;
            }
        }
    };

    public InterstitialAd(Activity act) {
        this.activity = new WeakReference<>(act);
        this.context = act.getApplicationContext();
        registerBroadCast();
    }

    public InterstitialAd setInterstitialAdListener(InterstitialAdListener interstitialAdListener) {
        this.userInterstitialAdListener = interstitialAdListener;
        return this;
    }

    public void showAd() {
        if (adLoaded) {
            adLoaded = false;
            activity.get().startActivity(new Intent(activity.get(), InterstitialActivity.class)
                    .putExtra(Constants.INTENT_EXTRA_MODEL, interstitialModel));
        } else {
            Log.i("ADAD SDK", "The interstitial wasn't loaded yet.");
        }
    }

    private void requestAd() {

        if (!adLoaded) {
            if (lastMillis == 0 || (System.currentTimeMillis() - lastMillis) > 30000) {
                lastMillis = System.currentTimeMillis();
                prepareUrl = PrepareUrl.getInstance(context);
                requestAd = RequestAd.getInstance(context);
                requestAd.setRequestAdListener(this);
                requestAd.sendRequestAd(prepareUrl.url(Constants.BASE_URL, Constants.TYPE_INTERSTITIAL, ""));
            }

        }
    }

    private void preLoadGlide(InterstitialModel interstitialModel) {

        Glide.with(context)
                .load(R.drawable.video_ic_skip)
                .preload();

        Glide.with(context)
                .load(interstitialModel.getLogoAdad())
                .preload();

        Glide.with(context)
                .load(interstitialModel.getImgUrl())
                .preload();

    }

    private void registerBroadCast() {

        LocalBroadcastManager
                .getInstance(context)
                .registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTERSTITIAL_AD));
    }

    public InterstitialAd setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public boolean isAdLoaded() {
        return adLoaded;
    }

    public void prepareAd() {
        requestAd();
    }

    @Override
    public void onResponse(AdType adType, BaseJsonModel baseJsonModel) {

        interstitialModel = (InterstitialModel) baseJsonModel;
        preLoadGlide(interstitialModel);
        adLoaded = true;
        userInterstitialAdListener.onAdLoaded(InterstitialAd.this);

    }

    @Override
    public void onError(int error) {
        lastMillis = 0;
        if (userInterstitialAdListener != null) {
            userInterstitialAdListener.onAdFailedToLoad(error);
        }
    }
}
