package ir.adad.adadsdkv6.utils;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;


import java.util.Locale;

import ir.adad.adadsdkv6.BuildConfig;

class LocationMethods {

    static private LocationMethods mInstance;
    private final long LOCATION_REFRESH_TIME = 100;
    private final float LOCATION_REFRESH_DISTANCE = 1000;

    private String ADAD_LOCATION = "adad_location_sharedPref";
    private String TAG = getClass().getSimpleName();
    private Context mContext;
    private boolean mSuppressBazaarErrors = false;
    private LocationManager locationManager;
    private SharedPreferences preferences;
    private Criteria criteria;
    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            String mLastLocation = String.format(
                    Locale.ENGLISH,
                    "%f_%f",
                    location.getLatitude(),
                    location.getLongitude());

            preferences.edit()
                    .putString(ADAD_LOCATION, mLastLocation)
                    .apply();


        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public LocationMethods(Context context) {

        mContext = context.getApplicationContext();
        preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        locationManager = (LocationManager) context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        criteria = createCoarseCriteria();

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria, true), LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, mLocationListener);
        } else {
            Log.i(TAG, "LocationMethods: NO PERMISSION GRANTED ");
        }

    }

    String getDeviceLocation(String bazaarUri) {

        String deviceLocation = getLocationFromBazaar(bazaarUri);
        if (deviceLocation != null) {
            return deviceLocation;
        }
        deviceLocation = getLocationFromAndroid();
        return deviceLocation != null ? deviceLocation : "";
    }

    private String getLocationFromBazaar(String bazaarUri) {
        ContentResolver contentResolver = mContext.getContentResolver();
        Uri bazaarUidUri = Uri.parse(bazaarUri);
        String loc = null;

        try {
            Cursor cursor = contentResolver.query(bazaarUidUri, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                loc = cursor.getString(0);
            }
        } catch (Exception ignored) {
        }

        if (!mSuppressBazaarErrors) {
            mSuppressBazaarErrors = true;
        }
        return loc;
    }

    private String getLocationFromAndroid() {

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        Location finalLoc ;
        String result = null;

        finalLoc = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));

        if (finalLoc != null) {
            result = String.format(
                    Locale.ENGLISH,
                    "%f_%f",
                    finalLoc.getLatitude(),
                    finalLoc.getLongitude());

        } else if (preferences.getString(ADAD_LOCATION, null) != null) {
            result = preferences.getString(ADAD_LOCATION, null);

            locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria, true), LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, mLocationListener);

        }

        return result;
    }

    public Criteria createCoarseCriteria() {

        Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_COARSE);
        c.setAltitudeRequired(false);
        c.setBearingRequired(false);
        c.setSpeedRequired(false);
        c.setCostAllowed(true);
        c.setPowerRequirement(Criteria.POWER_HIGH);
        return c;

    }

    public void onStop() {

        locationManager.removeUpdates(mLocationListener);
    }

}
