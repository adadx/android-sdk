package ir.adad.adadsdkv6.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deeas on 11/12/17.
 */

public class BaseJsonModel implements Parcelable {

    private String contentType;
    private long adId;
    private String size;
    private String version;
    private String device;
    private String logoAdad;
    private int refreshInterval;



    public BaseJsonModel(JSONObject jsonObject) throws JSONException {

        this.contentType = jsonObject.getString("contentType");
        this.logoAdad = jsonObject.getString("adadLogo");
        this.adId = jsonObject.getLong("adId");
        this.size = jsonObject.getString("size");
        this.version = jsonObject.getString("version");
        this.device = jsonObject.getString("device");
        this.refreshInterval = jsonObject.getInt("refreshInterval");

    }

    public String getContentType() {
        return contentType;
    }

    public long getAdId() {
        return adId;
    }

    public String getSize() {
        return size;
    }

    public String getVersion() {
        return version;
    }

    public String getDevice() {
        return device;
    }

    public String getLogoAdad() {
        return logoAdad;
    }

    public int getRefreshInterval() {
        return refreshInterval;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.contentType);
        dest.writeLong(this.adId);
        dest.writeString(this.size);
        dest.writeString(this.version);
        dest.writeString(this.device);
        dest.writeString(this.logoAdad);
        dest.writeInt(this.refreshInterval);
    }

    protected BaseJsonModel(Parcel in) {
        this.contentType = in.readString();
        this.adId = in.readLong();
        this.size = in.readString();
        this.version = in.readString();
        this.device = in.readString();
        this.logoAdad = in.readString();
        this.refreshInterval = in.readInt();
    }

    public static final Creator<BaseJsonModel> CREATOR = new Creator<BaseJsonModel>() {
        @Override
        public BaseJsonModel createFromParcel(Parcel source) {
            return new BaseJsonModel(source);
        }

        @Override
        public BaseJsonModel[] newArray(int size) {
            return new BaseJsonModel[size];
        }
    };
}
