package ir.adad.adadsdkv6.models.videoJson;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import ir.adad.adadsdkv6.models.BaseJsonModel;

import static ir.adad.adadsdkv6.utils.Constants.TYPE_HYBRID;

/**
 * Created by saeed on 11/1/17.
 */

public class VideoModel extends BaseJsonModel {

    private String videoCheckUrl;
    private double videoDuration;
    private String video;
    private String videoPlayedUrl;


    private String impUrl;
    private String richBottomBarColor;
    private String richButtonTextColor;
    private String logo;
    private String richButtonColor;
    private String richTextColor;
    private String text;
    private String imageUrl;
    private String hitUrl;
    private String targetType;
    private String buttonIcon;

    private boolean hasRich = false;


    public boolean hasRich() {
        return hasRich;
    }

    public VideoModel(JSONObject jsonObject) throws JSONException {
        super(jsonObject);
        this.videoCheckUrl = jsonObject.getString("videoCheckUrl");
        this.videoDuration = jsonObject.getDouble("video_duration");
        this.video = jsonObject.getString("video");
        this.videoPlayedUrl = jsonObject.getString("videoPlayedUrl");

        if (getContentType().equals(TYPE_HYBRID)) {
            hasRich = true;

            this.impUrl = jsonObject.getString("impUrl");
            this.richBottomBarColor = jsonObject.getString("richBottomBarColor");
            this.richButtonTextColor = jsonObject.getString("richButtonTextColor");
            this.logo = jsonObject.getString("logoUrl");
            this.richButtonColor = jsonObject.getString("richButtonColor");
            this.richTextColor = jsonObject.getString("richTextColor");
            this.text = jsonObject.getString("text");
            this.imageUrl = jsonObject.getString("imageUrl");
            this.hitUrl = jsonObject.getString("hitUrl");
            this.targetType = jsonObject.getString("targetType");
            this.buttonIcon = jsonObject.getString("buttonIcon");

        }
    }

    public String getVideoCheckUrl() {
        return videoCheckUrl;
    }

    public String getButtonIcon() {
        return buttonIcon;
    }

    public double getVideoDuration() {
        return videoDuration;

    }

    public String getVideo() {
        return video;
    }

    public String getVideoPlayedUrl() {
        return videoPlayedUrl;
    }

    public String getImpUrl() {
        return impUrl;
    }

    public String getRichBottomBarColor() {
        return richBottomBarColor;
    }

    public String getRichButtonTextColor() {
        return richButtonTextColor;
    }

    public String getLogo() {
        return logo;
    }

    public String getRichButtonColor() {
        return richButtonColor;
    }

    public String getRichTextColor() {
        return richTextColor;
    }

    public String getText() {
        return text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getHitUrl() {
        return hitUrl;
    }

    public String getTargetType() {
        return targetType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.videoCheckUrl);
        dest.writeDouble(this.videoDuration);
        dest.writeString(this.video);
        dest.writeString(this.videoPlayedUrl);
        dest.writeString(this.impUrl);
        dest.writeString(this.richBottomBarColor);
        dest.writeString(this.richButtonTextColor);
        dest.writeString(this.logo);
        dest.writeString(this.richButtonColor);
        dest.writeString(this.richTextColor);
        dest.writeString(this.text);
        dest.writeString(this.imageUrl);
        dest.writeString(this.hitUrl);
        dest.writeString(this.targetType);
        dest.writeString(this.buttonIcon);
        dest.writeByte(this.hasRich ? (byte) 1 : (byte) 0);
    }

    protected VideoModel(Parcel in) {
        super(in);
        this.videoCheckUrl = in.readString();
        this.videoDuration = in.readDouble();
        this.video = in.readString();
        this.videoPlayedUrl = in.readString();
        this.impUrl = in.readString();
        this.richBottomBarColor = in.readString();
        this.richButtonTextColor = in.readString();
        this.logo = in.readString();
        this.richButtonColor = in.readString();
        this.richTextColor = in.readString();
        this.text = in.readString();
        this.imageUrl = in.readString();
        this.hitUrl = in.readString();
        this.targetType = in.readString();
        this.buttonIcon = in.readString();
        this.hasRich = in.readByte() != 0;
    }

    public static final Creator<VideoModel> CREATOR = new Creator<VideoModel>() {
        @Override
        public VideoModel createFromParcel(Parcel source) {
            return new VideoModel(source);
        }

        @Override
        public VideoModel[] newArray(int size) {
            return new VideoModel[size];
        }
    };
}
