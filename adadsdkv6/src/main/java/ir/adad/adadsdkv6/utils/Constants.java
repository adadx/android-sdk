package ir.adad.adadsdkv6.utils;


public class Constants {


    public static final String VIDEO_TYPE_FULLSC = "l87tkm6jn5e";
    public static final String VIDEO_TYPE_PRELOAD = "4t56578k7";
    public static final String INTERSTITIAL_AD = "75kuj6y";
    public static final String INTENT_ADAD_CHECK = "asadfas";
    public static final String INTENT_EXTRA_STATE = "aasdfcsqe";
    public static final String INTENT_EXTRA_ERROR = "jeqcgrew";
    public static final String INTENT_EXTRA_TYPE = "1f5hj6vb4";
    public static final String INTENT_EXTRA_MODEL = "213rf4jh6";
    public static final String INTENT_EXTRA_SKIP_TIME = "FWF2THe";
    public static final String EVENT_SKIPPED = "FWF7JUJ";
    public static final String EVENT_COMPLETED = "23rf4j64t";
    public static final String EVENT_ACTIVITY_STOPPED = "qwevg14ud";
    public static final String EVENT_AD_PLAYED = "a3qtg6d";
    public static final String EVENT_API_AD_PLAYED = "4wh686d";
    public static final String EVENT_AD_CLOSED = "q34tg6";
    public static final String EVENT_VIDEO_RICH_VIEW = "q4gk89r";
    public static final String EVENT_AD_CLICK = "1r8767yt";
    public static final String EVENT_ERROR = "4tg54657i6y";
    public static final String TYPE_ALLVIDEO = "all_video";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_HYBRID = "hybrid";
    public static final String TYPE_BANNER = "banner";
    public static final String TYPE_IMAGEBANNER = "image";
    public static final String TYPE_TEXTBANNER = "text";
    public static final String TYPE_INTERSTITIAL = "interstitial";
    public static final String TYPE_IMAGEINTERSTITIAL = "interstitial-image";
    public static final String BAZAAR_DEFAULT_ID_URI = "content://com.farsitel.bazaar/info/get_uid";
    public static final String BAZAAR_DEFAULT_LOCATION_URI = "content://com.farsitel.bazaar/info/get_jaw";
    public static final String BASE_URL = "http://adad.ir/adview/";
    public static final String ADAD_LOCATION = "adad_location_sharedPref";

    public static final String CONTENTTYPE = "contentType";


}
